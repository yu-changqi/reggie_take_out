/*获取分类列表数据*/
const getCategoryList = (params) => {
    return $axios({
        url: '/category/list',
        method: 'get',
        params
    })
}
/*获取逻辑删除菜品列表*/
const getLogicDelDishList = (params) => {
    return $axios({
        url: '/recycle/tombstone',
        method: 'post',
        data: { ...params }
    })
}

// 删除接口
const deleteDish = (ids) => {
    return $axios({
        url: '/recycle/romove',
        method: 'delete',
        params: { ids }
    })
}
//恢复接口
const restore = (ids) => {
    return $axios({
        url: '/recycle/restore',
        method: 'get',
        params: { ids }
    })
}