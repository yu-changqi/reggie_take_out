package com.itheima.vo;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.itheima.entity.SetmealDish;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: jingHaoHao
 * @Desc: 套餐--套餐视图对象
 * @Date: 2022/6/5 15:51
 */
@Data
@TableName("setmeal")
public class SetmealVO implements Serializable {

    /**
     *套餐管理-菜品
     */
    private List<SetmealDish> setmealDishes;

    /**
     *套餐管理主键-id
     */
    private Long id;
    /**
     *菜品分类id
     */
    private Long categoryId;
    /**
     *套餐名称
     */
    private String name;
    /**
     *套餐价格
     */
    private BigDecimal price;
    /**
     *套餐管理状态 0:停用 1:启用
     */
    private Integer status;
    /**
     *编码
     */
    private String code;
    /**
     *描述信息
     */
    private String description;
    /**
     *图片
     */
    private String image;
    /**
     *创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /**
     *更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    public LocalDateTime updateTime;
    /**
     *创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;
    /**
     *修改人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
    /**
     *逻辑删除- 0-未删除 1-删除
     */
    @TableLogic
    private Integer isDeleted;


}
