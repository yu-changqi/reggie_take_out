package com.itheima.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Eleike
 * @Desc: 分页数据集返回封装
 * @Date: 2022/6/5 15:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResult {
    /**
     * 状态码
     */
    private String code;
    /**
     * 状态信息
     */
    private String msg;
    /**
     * 数据
     */

    private Object data;
    /**
     * 当前页
     */

    private Long page;
    /**
     * 每页记录数
     */

    private Long pageSize;
    /**
     * 总页数
     */

    private Long totalPage;
    /**
     * 总记录数
     */

    private Long total;

    public PageResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public PageResult(String code, String msg, Long page) {
        this.code = code;
        this.msg = msg;
        this.page = page;
    }

    public PageResult(String code, String msg, Object data, Long total) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.total = total;
    }

}

