package com.itheima.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: qiChangYue
 * @Desc:回收站--数据传输对象
 * @Date: 2022/6/6 10:23
 */
@Data
public class RecycleDTO implements Serializable {

    /*
    当前页
     */
    private	Integer page;
    /**
     分页条数
     */
    private	Integer size;
    /**
      分类id
     */
    private	Long categoryId;
    /**
     名称
     */
    private	String name;
    /**
     修改日期时间
     */
    private LocalDateTime[] dateRange = new LocalDateTime[]{};
    /**
     开始时间
     */
    private LocalDateTime startTime;
    /**
     结束时间
     */
    private LocalDateTime endTime;
}
