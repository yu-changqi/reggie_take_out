package com.itheima.constant;

/**
 * @Author: Eleike
 * @Desc: 业务功能模块状态提示信息
 * @Date: 2022/6/5 15:13
 */
public class BusinessMessageConstant {
    ////////////////////////////////////公用提示信息//////////////////////////////////////
    /*================================分页查询==================================*/
    /**
     * 分页查询成功提示信息
     */
    public static final String PAGE_SELECT_SUCCESS = "分页查询成功";
    /**
     * 分页查询失败提示信息
     */
    public static final String PAGE_SELECT_ERROR = "分页查询失败，请重试";
    /*================================文件上传==================================*/
    /**
     * 文件上传成功提示
     */
    public static final String UPLOAD_FILE_SUCCESS = "文件上传成功！";
    /**
     * 上传文件为空提示
     */
    public static final String UPLOAD_FILE_NOT_SELECTED = "文件未选择，请先选择文件！";
    /**
     * 上传路径没有写权限提示
     */
    public static final String UPLOAD_NOT_HAVE_WRITER_PERMISSION = "上传目录没有写权限！";
    /**
     * 上传目录名不正确提示
     */
    public static final String UPLOAD_DIR_IS_INCORRECT = "目录名不正确！";
    /**
     * 上传文件爱你大小超过限制提示
     */
    public static final String UPLOAD_FILE_SIZE_EXCEEDS_LIMIT = "上传文件大小超过限制！";
    /**
     * 上传文件扩展名不合理提示
     */
    public static final String UPLOAD_FILE_EXT_IS_NOT_ALLOWED = "上传文件扩展名是不允许的扩展名！";

    ////////////////////////////////////登录查询提示信息//////////////////////////////////////
    /**
     * 登陆失败提示信息
     */
    public static final String EMPLOYEE_LOGIN_ERROR = "登陆失败";
    /**
     * 登陆成功提示信息
     */
    public static final String EMPLOYEE_LOGIN_SUCCESS = "登陆成功";
    /**
     * 退出失败提示信息
     */
    public static final String EMPLOYEE_LOGIN_OUT_ERROR = "退出失败";
    /**
     * 退出成功提示信息
     */
    public static final String EMPLOYEE_LOGIN_OUT_SUCCESS = "退出成功";

    ////////////////////////////////////首页概况提示信息//////////////////////////////////////
    /*================================菜品套餐占比饼图==================================*/
    /**
     * 菜品套餐数量查询成功
     */
    public static final String HOME_SELECT_DISH_AND_SETMEAL_COUNT_SUCCESS= "菜品套餐数量查询成功";
    /**
     * 菜品套餐数量查询失败
     */
    public static final String HOME_SELECT_DISH_AND_SETMEAL_COUNT_ERROR= "菜品套餐数量查询失败，请刷新重试";
    /*=================================新增用户折线图===================================*/
    /**
     * 新增用户数量获取成功
     */
    public static final String HOME_SELECT_NEW_USER_COUNT_SUCCESS= "新增用户数量获取成功";
    /**
     * 新增用户数量获取失败
     */
    public static final String HOME_SELECT_NEW_USER_COUNT_ERROR= "新增用户数量获取失败，请刷新重试";
    /*=================================首页轮播图===================================*/
    /**
     * 首页轮播图获取成功提示信息
     */
    public static final String HOME_SELECT_HOME_IMAGE_SUCCESS= "轮播图图片获取成功";
    /**
     * 首页轮播图获取失败提示信息
     */
    public static final String HOME_SELECT_HOME_IMAGE_ERROR= "轮播图图片获取失败，请重试!";
    /**
     * 首页轮播图修改成功提示信息
     */
    public static final String HOME_UPDATE_HOME_IMAGE_SUCCESS= "图片修改成功";
    /**
     * 首页轮播图修改失败提示信息
     */
    public static final String HOME_UPDATE_HOME_IMAGE_ERROR= "图片修改失败，请重试!";

    ////////////////////////////////////员工模块提示信息//////////////////////////////////////
    /**
     *员工信息页面显示成功
     */
    public static final String EMP_SEARCH_SUCCESS="员工信息页面显示成功";
    /**
     *新增员工成功
     */
    public static final String EMP_SAVE_SUCCESS="新增员工成功";
    /**
     *新增员工失败
     */
    public static final String EMP_SAVE_ERROR="新增员工失败";
    /**
     *查询员工成功
     */
    public static final String EMP_GET_SUCCESS="查询员工成功";
    /**
     *查询员工失败
     */
    public static final String EMP_GET_ERROR="查询员工失败";
    /**
     *修改员工信息失败
     */
    public static final String EMP_UPDATE_ERROR="修改员工信息失败";
    /**
     *修改员工信息成功
     */
    public static final String EMP_UPDATE_SUCCESS="修改员工信息成功";
    /**
     *删除员工失败
     */
    public static final String EMP_DEL_ERROR="删除员工失败";
    /**
     *删除员工成功
     */
    public static final String EMP_DEL_SUCCESS="删除员工成功";

    ////////////////////////////////////分类管理 消息提示//////////////////////////////////////
    /**
    * 新增菜品分类成功消息提示
    */
    public static final String CATEGORY_CREATE_DISH_CLASSIFICATION_OK = "新增分类成功";
    /**
    * 新增菜品分类失败消息提示
    */
    public static final String CATEGORY_CREATE_DISH_CLASSIFICATION_ERROR = "新增分类失败,分类名称或排序已存在";
    /**
    * 修改分类信息成功消息提示
    */
    public static final String CATEGORY_UPDATE_CATEGORY_OK = "修改分类信息成功";
    /**
    * 修改分类信息失败消息提示
    */
    public static final String CATEGORY_UPDATE_CATEGORY_ERROR = "修改分类信息失败";
    /**
    * 删除分类信息成功
    */
    public static final String CATEGORY_DELETE_CATEGORY_OK = "删除分类信息成功";
    /**
    * 删除分类信息失败
    */
    public static final String CATEGORY_DELETE_CATEGORY_ERROR = "删除分类信息失败";
    /**
    * 根据分类类型(Type) 查询分类信息成功
    */
    public static final String CATEGORY_SELECT_CATEGORY_BY_TYPE_OK = "根据分类类型(Type) 查询分类信息成功";
    /**
    * 根据分类类型(Type) 查询分类信息失败
    */
    public static final String CATEGORY_SELECT_CATEGORY_BY_TYPE_ERROR = "根据分类类型(Type) 查询分类信息失败";

    /////////////////////////////////////菜品专用提示信息////////////////////////////////////////
    /*================================菜品分页查询==================================*/
    /**
    * 菜品分页查询成功提示信息
    */
    public static final String DISH_PAGE_SELECT_SUCCESS = "菜品信息分页查询成功";
    /**
    * 菜品分页查询失败提示信息
    */
    public static final String DISH_PAGE_SELECT_ERROR = "菜品信息分页查询失败，请重试";
    /*================================菜品添加模块==================================*/
    /**
    * 菜品添加成功提示信息
    */
    public static final String DISH_ADD_SUCCESS = "菜品信息添加成功";
    /**
    * 菜品添加失败提示信息
    */
    public static final String DISH_ADD_ERROR = "菜品信息添加失败，请重试";
    /**
    * 菜品添加重复提示信息
    */
    public static final String DISH_ADD_ERROR_REPEAT = "菜品名称信息添加重复，请重试";
    /**
     * 菜品添加重复提示信息
     */
    public static final String DISH_ADD_ERROR_NAME_EMPTY = "菜品名称格式不正确！添加失败，请输入正确菜品名称";
    /*================================菜品修改模块==================================*/
    /**
    * 菜品数据回显失败
    */
    public static final String DISH_DATA_ECHO_ERROR = "菜品数据回显失败，请重试";
    /**
    * 菜品数据回显成功
    */
    public static final String DISH_DATA_ECHO_SUCCESS = "菜品数据回显成功";
    /**
    * 菜品信息更新失败提示信息
    */
    public static final String DISH_UPDATE_ERROR = "菜品信息更新失败，请重试";
    /**
    * 菜品信息更新成功提示信息
    */
    public static final String DISH_UPDATE_SUCCESS = "菜品信息更新成功";
    /**
    * 菜品状态更新失败
    */
    public static final String DISH_CHANGESTATUS_ERROR = "菜品状态更新失败，请重试";
    /**
    * 菜品状态更新成功
    */
    public static final String DISH_CHANGESTATUS_SUCCESS = "菜品状态更新成功";
    /*================================菜品删除模块==================================*/
    /**
    * 批量删除菜品信息失败
    */
    public static final String DISH_DELETE_ERROR = "批量删除菜品信息失败，请重试";
    /**
    * 批量删除菜品信息成功
    */
    public static final String DISH_DELETE_SUCCESS = "批量删除菜品信息成功";
    /**
    * 根据分类id获取菜品信息失败
    */
    public static final String DISH_LIST_ERROR = "根据分类id获取菜品信息失败";
    /**
    * 根据分类id获取菜品信息成功
    */
    public static final String DISH_LIST_SUCCESS = "根据分类id获取菜品信息成功";

     ////////////////////////////////////套餐管理 消息提示//////////////////////////////////////
    /**
     * 未接收到套餐数据
     */
    public static final String SETMEAL_DATA_NO_UPLOAD="未接收到所需要的数据";
    /**
     * 套餐数据重复
     */
    public static final String SETMEAL_DATA_PACKAGE="套餐数据重复";
    /**
     * 套餐数据添加成功
     */
    public static final String SETMEAL_DATA_ADD_SUCCESS="套餐添加成功";
    /**
     * 套餐数据添加失败
     */
    public static final String SETMEAL_DATA_ADD_ERROR="套餐数据添加失败";
    /**
     * 套餐查询成功
     */
    public static final String SETMEAL_SELECT_SUCCESS="套餐查询成功";
    /**
     *  套餐查询失败
     */
    public static final String SETMEAL_SELECT_ERROR="套餐查询失败";

    /**
     *套餐删除提示信息失败
     */
    public static final String SETMEAL_DELETE_ERROR = "套餐删除失败!";
    /**
     *套餐状态修改信息成功
     */
    public static final String SETMEAL_DELETE_SUCCESS = "套餐删除成功！";

    /**
     *套餐状态修改信息失败
     */
    public static final String SETMEAL_POST_STATE_ERROR = "套餐状态修改失败!";
    /**
     *套餐状态修改信息成功
     */
    public static final String SETMEAL_POST_STATE_SUCCESS = "套餐状态修改成功！";
    /**
     * 套餐修改失败
     */
    public static final String SETMEAL_PUT_ERROR = "套餐修改失败";
    /**
     * 套餐修改失败
     */
    public static final String SETMEAL_PUT_SUCCESS = "套餐修改成功";

    ////////////////////////////////////订单明细 消息提示//////////////////////////////////////
    /**
    *  修改订单状态成功提示
    */
    public static final String ORDER_UPDATE_SUCCESS = "订单状态修改成功";
    /**
    *  修改订单状态失败提示
    */
    public static final String ORDER_UPDATE_ERROR = "订单状态修改失败";

    ////////////////////////////////////订单明细 消息提示//////////////////////////////////////
    /**
     * 获取菜品
     */
    public static final String DISH_GET_SUCCESS = "获取菜品信息成功!";
    public static final String DISH_GET_ERRO = "获取菜品信息失败!";
    /**
     * 菜品恢复
     */
    public static final String DISH_RESTORE_SUCCESS = "菜品恢复成功!";
    public static final String DISH_RESTORE_ERRO = "菜品恢复失败!";

    /**
     * 菜品删除
     */
    public static final String DISH_ROMOVE_ERRO = "菜品删除失败!";
    public static final String DISH_ROMOVE_SUCCESS = "菜品删除成功!";
}
