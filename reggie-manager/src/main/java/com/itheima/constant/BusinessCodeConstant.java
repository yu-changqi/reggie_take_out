package com.itheima.constant;

/**
 * @Author: Eleike
 * @Desc: 业务功能模块状态码
 * @Date: 2022/6/5 15:04
 */
public class BusinessCodeConstant {
    /**
     * 成功执行状态码
     */
    public static final String OK = "00000";

    //////////////////////////////////////用户状态码A0300~A0399///////////////////////////////////////////
    /*=================================登录查询===================================*/
    /**
     * 失败执行状态码
     */
    public static final String LOGIN_ERROR="A0301";
    /**
     * 用户未登录状态码
     */
    public static final String EMPLOYEE_NOT_LOGIN_IN="A0302";
    //////////////////////////////////////公用状态码A0000~A0099///////////////////////////////////////////
    /*=================================分页查询===================================*/
    /**
     * 分页查询失败
     */
    public static final String PAGE_SELECT_ERROR = "A0001";

    /*================================文件上传==================================*/
    /**
     * 文件不存在
     */
    public static final String UPLOAD_FILE_NOT_SELECTED = "A0011";
    /**
     * 上传目录没有写权限
     */
    public static final String UPLOAD_NOT_HAVE_WRITER_PERMISSION = "A0012";
    /**
     * 目录名不正确
     */
    public static final String UPLOAD_DIR_IS_INCORRECT = "A0013";
    /**
     * 文件大小超出限制
     */
    public static final String UPLOAD_FILE_SIZE_EXCEEDS_LIMIT = "A0014";
    /**
     * 上传文件扩展名不允许
     */
    public static final String UPLOAD_FILE_EXT_IS_NOT_ALLOWED = "A0015";

    //////////////////////////////////////首页概况A0100~A0199///////////////////////////////////////////
    /*=================================菜品套餐占比饼图===================================*/
    /**
     * 菜品套餐数量查询失败
     */
    public static final String HOME_SELECT_DISH_AND_SETMEAL_COUNT_ERROR= "A0101";
    /*=================================新增用户折线图===================================*/
    /**
     * 新增用户数量获取失败
     */
    public static final String HOME_SELECT_NEW_USER_COUNT_ERROR= "A0111";
    /*=================================首页轮播图===================================*/
    /**
     * 首页轮播图获取失败
     */
    public static final String HOME_SELECT_HOME_IMAGE_ERROR= "A0121";
    /**
     * 首页轮播图修改失败
     */
    public static final String HOME_UPDATE_HOME_IMAGE_ERROR= "A0122";

    //////////////////////////////////////员工管理状态码A0300~A0399///////////////////////////////////////////
    /**
     * 新增员工失败
     */
    public static final String EMP_SAVE_ERROR = "A0300";
    /**
     * 修改员工信息失败
     */
    public static final String EMP_UPDATE_ERROR = "A0301";
    /**
     * 修改员工信息成功
     */
    public static final String EMP_UPDATE_SUCCESS = "A0302";
    /**
     *查询员工信息失败
     */
    public static final String EMP_GET_ERROR="A0303";
    /**
     *删除员工信息失败
     */
    public static final String EMP_DEL_ERROR="A0304";

    //////////////////////////////////////分类管理状态码A0400~A0499///////////////////////////////////////////
    /**
    * 新增分类失败状态码
    */
    public static final String CATEGORY_CREATE_DISH_CLASSIFICATION_ERROR = "A0411";
    /**
    * 修改分类信息失败状态码
    */
    public static final String CATEGORY_UPDATE_CATEGORY_ERROR = "A0421";
    /**
    * 删除分类信息失败状态码
    */
    public static final String CATEGORY_DELETE_CATEGORY_ERROR = "A0431";
    /**
    * 根据分类类型(Type) 查询分类信息失败状态码
    */
    public static final String CATEGORY_SELECT_CATEGORY_BY_TYPE_ERROR = "A0441";

    //////////////////////////////////////菜品管理状态码A0500~A0599///////////////////////////////////////////
    /**
    * 添加菜品失败
    */
    public static final String DISH_ADD_ERROR = "A0500";
    /**
    * 菜品数据回显失败
    */
    public static final String DISH_DATA_ECHO_ERROR = "A0501";
    /**
    * 更新菜品失败
    */
    public static final String DISH_UPDATE_ERROR = "A0502";
    /**
    * 更新菜品状态失败
    */
    public static final String DISH_CHANGESTATUS_ERROR = "A0503";
    /**
    * 批量删除菜品信息失败
    */
    public static final String DISH_DELETE_ERROR = "A0504";
    /**
    * 根据分类id获取菜品信息失败
    */
    public static final String DISH_LIST_ERROR = "A0505";

    //////////////////////////////////////套餐管理状态码A0600~A0699///////////////////////////////////////////
    /**
     * 套餐新增失败状态码
     */
    public static final String SETMEAL_ADD_ERROR = "A0601";
    /**
     * 套餐修改失败状态码
     */
    public static final String SETMEAL_PUT_ERROR = "A0602";
    /**
     * 套餐删除失败状态码
     */
    public static final String SETMEAL_DELETE_ERROR = "A0603";
    /**
     * 套餐修改状态失败状态码
     */
    public static final String SETMEAL_POST_SATE_ERROR = "A0604";
    /**
     * 套餐查询失败
     */
    public static final String SETMEAL_SELECT_ERROR = "A0605";
    /**
     * 套餐数据未传入
     */
    public static final String SETMEAL_DATA_NO_UPLOAD = "A0610";
    /**
     * 套餐数据添加数据库失败
     */
    public static final String SETMEAL_DATA_ADD_ERROR= "A0611";
    /**
     * 套餐数据重复
     */
    public static final String SETMEAL_DATA_PACKAGE= "A0612";

    //////////////////////////////////////订单管理状态码A0700~A0799///////////////////////////////////////////
    /**
    * 订单修改成功状态码
    */
    public static final String ORDER_UPDATE_SUCCESS = "A0701";

    public static final String ORDER_UPDATE_ERROR = "A0702";

    //////////////////////////////////////回收站状态码A0800~A0899///////////////////////////////////////////

    /**
     * 回收站查询失败
     */
    public static final String RECYCLE_SELECT_ERROR = "A0801";

    /**
     * 回收站删除数据失败
     */
    public static final String RECYCLE_DELETE_ERROR = "A0802";
    /**
     * 回收站还原数据失败
     */
    public static final String RECYCLE_REDUCTION_ERROR = "A0803";

}
