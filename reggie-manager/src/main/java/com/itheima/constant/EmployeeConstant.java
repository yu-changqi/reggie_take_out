package com.itheima.constant;

/**
 * @Author: Eleike
 * @Desc: 员工模块-状态常量
 * @Date: 2022/6/5 16:26
 */
public class EmployeeConstant {
    /**
     * 员工状态-启用
     */
    public static final int EMPLOYEE_STATUS_ENABLE = 1;
    /**
     * 员工状态-禁用
     */
    public static final int EMPLOYEE_STATUS_DISABLE = 0;
    /**
     * 员工登录的业务主键
     */
    public static final String EMPLOYEE_LOGIN = "EMPLOYEE_LOGIN_";
    /**
     * 员工登录-Cookie的key名称
     */
    public static final String EMPLOYEE_TOKIN = "employeeInfo";
    /**
     * 员工-Cookie立即过期
     */
    public static final Integer EMPLOYEE_TOKIN_DEAD = 0;
    /**
     * 员工添加默认密码
     */
    public static final String EMPLOYEE_DEFAULT_PASSWORD = "123456";
    /**
     * cookie最大存活时间
     */
    public static final Integer TOKEN_EXPIRE = 30*60;
}
