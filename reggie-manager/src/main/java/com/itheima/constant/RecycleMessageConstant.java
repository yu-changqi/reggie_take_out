package com.itheima.constant;
/**
 * @Author: qiChangYue
 * @Desc:回收站消息常量信息提示
 * @Date: 2022/6/6 12:25
 */

public class RecycleMessageConstant {
    /**
     * 获取菜品
     */
    public static final String DISH_GET_SUCCESS = "获取菜品信息成功!";
    public static final String DISH_GET_ERRO = "获取菜品信息失败!";
    /**
     * 菜品恢复
     */
    public static final String DISH_RESTORE_SUCCESS = "菜品恢复成功!";
    public static final String DISH_RESTORE_ERRO = "菜品恢复失败!";

    /**
     * 菜品删除
     */
    public static final String DISH_ROMOVE_ERRO = "菜品删除失败!";
    public static final String DISH_ROMOVE_SUCCESS = "菜品删除成功!";
}
