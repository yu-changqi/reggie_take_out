package com.itheima.constant;

/**
 * @Author: Eleike
 * @Desc: 系统错误提示信息
 * @Date: 2022/6/5 15:13
 */
public class SystemMessageConstant {
    /**
     * 系统未知错误提示
     */
    public static final String SYSTEM_UNKNOW_ERROR= "系统超时，请重试";
    /**
     * 错误提示
     */
    public static final String BUSINESS_ERR_CANNOT_FIND= "业务超时，请重试";
    /**
     * 分类 名称信息 已存在
     */
    public static final String CATEGORY_NAME_IS_EXIST= "分类名称或排序已存在";
}
