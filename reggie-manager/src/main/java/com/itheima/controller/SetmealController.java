package com.itheima.controller;

import com.itheima.common.PageResult;
import com.itheima.common.Result;
import com.itheima.dto.SetmealDTO;
import com.itheima.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: jingHaoHao
 * @Desc:套餐管理系统--Controller层
 * @Date: 2022/6/6 11:02
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    /**
     * 功能描述  : 套餐模块-分页查寻
     * @param page
     * @param pageSize
     * @return :PageResult
     */
    @GetMapping("/page")
    public PageResult pagingQuery(@RequestParam("page") Integer page,
                                  @RequestParam("pageSize") Integer pageSize,
                                  @RequestParam(value = "name", required = false) String name) {
        return  setmealService.pagingQuery(page, pageSize, name);
    }

    /**
     * 功能描述  :套餐模块-添加套餐
     * @param setmealDTO
     * @return :Result
     */
    @PostMapping
    public Result addSetmeal(@RequestBody SetmealDTO setmealDTO){
        return setmealService.addSetmeal(setmealDTO);
    }

    /**
     * 功能描述  : 套餐模块-逻辑删除
     * @param ids
     * @return : Result
     */
    @DeleteMapping
    public Result deleteByIds(@RequestParam("ids") Long[] ids) {
        return  setmealService.deleteByIds(ids);
    }

    /**
     * 功能描述  :套餐模块-套餐状态修改
     * @param status
     * @param ids
     * @return : com.itheima.common.Result
     */
    @PostMapping("/status/{status}")
    public Result findSetmealListByCategoryId(@PathVariable("status") Integer status, @RequestParam("ids") Long[] ids) {
        return setmealService.findSetmealListByCategoryId(status,ids);
    }

    /**
     * 功能描述  :数据回显
     * @param id
     * @return : com.itheima.common.Result
     */
    @GetMapping("/{id}")
    public Result dataEcho(@PathVariable("id") Long id){
        return setmealService.dataEcho(id);
    }


    /**
     * 功能描述  : 套餐修改
     * @param setmealDTO
     * @return : com.itheima.common.Result
     */
    @PutMapping
    public Result update(@RequestBody SetmealDTO setmealDTO){
        return setmealService.update(setmealDTO);
    }
}
