package com.itheima.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.PageResult;
import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.constant.EmployeeConstant;
import com.itheima.entity.Employee;
import com.itheima.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: liuxinwei
 * @Desc:  用户-登录登出
 * @Date: 2022/6/5 17:56
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @PostMapping("/login")
    public Result login(@RequestBody Employee employee, HttpServletResponse response){
        Employee login = employeeService.login(employee);
        //登陆失败,给出提示
        if(login==null){
            return new Result(BusinessCodeConstant.LOGIN_ERROR, BusinessMessageConstant.EMPLOYEE_LOGIN_ERROR);
        }
        Long id = login.getId();
        String getId = id.toString();
        String token = EmployeeConstant.EMPLOYEE_LOGIN + login.getId();
        Cookie cookie = new Cookie(EmployeeConstant.EMPLOYEE_TOKIN, token);
        cookie.setMaxAge(EmployeeConstant.TOKEN_EXPIRE);
        cookie.setDomain("eleike.xyz");
        cookie.setPath("/");
        response.addCookie(cookie);

        redisTemplate.opsForValue().set(token,getId,3000, TimeUnit.SECONDS);

        Map<String, String> employeeMap = new HashMap<>(2);
        String username = login.getUsername();
        String image = login.getImage();
        employeeMap.put("username",username);
        employeeMap.put("image",image);
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.EMPLOYEE_LOGIN_SUCCESS,employeeMap);
    }
    @PostMapping("/logout")
    public Result logout(HttpServletRequest request, HttpServletResponse response){
        //1.获取要删除的redis的key（sessionId）
        String sid = null;
        Cookie[] cookies = request.getCookies();
        if(cookies==null){
            return new Result(BusinessCodeConstant.LOGIN_ERROR,BusinessMessageConstant.EMPLOYEE_LOGIN_OUT_ERROR);
        }
        for (Cookie cookie : cookies) {
            if(EmployeeConstant.EMPLOYEE_TOKIN.equals(cookie.getName())){
                sid = cookie.getValue();
            }
        }
      //删除redis中的令牌数据
        redisTemplate.delete(sid);
        //3.从cookie删除指定的数据
        Cookie cookie = new Cookie(EmployeeConstant.EMPLOYEE_TOKIN, "");
        cookie.setMaxAge(EmployeeConstant.EMPLOYEE_TOKIN_DEAD);
        cookie.setDomain("eleike.xyz");
        cookie.setPath("/");
        response.addCookie(cookie);
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.EMPLOYEE_LOGIN_OUT_SUCCESS);
    }

    @GetMapping("/page")
    public PageResult page(@RequestParam("page") Integer page, Integer pageSize, String name){
        Page<Employee> pageResult = employeeService.page(page,pageSize,name);
        //1.当前页面记录列表
        List<Employee> records = pageResult.getRecords();
        //2.总记录数
        long total = pageResult.getTotal();
        return new PageResult(BusinessCodeConstant.OK,BusinessMessageConstant.EMP_SEARCH_SUCCESS,records,total);
    }
    /**
     * 功能描述: 新增员工
     * @param employee
     * @return : com.itheima.common.Result
     */
    @PostMapping
    public Result save(@RequestBody Employee employee){
        int row = employeeService.save(employee);
        if(row>0){
            return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.EMP_SAVE_SUCCESS);
        }
        return new Result(BusinessCodeConstant.EMP_SAVE_ERROR,BusinessMessageConstant.EMP_SAVE_ERROR);
    }
    /**
     * 功能描述:根据id查询员工
     * @return : result
     * @param id
     */
    @GetMapping("/{id}")
    public Result findById(@PathVariable("id") Long id){
        Employee employees=employeeService.findById(id);
        if(employees==null){
            return new Result(BusinessCodeConstant.EMP_GET_ERROR,BusinessMessageConstant.EMP_GET_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.EMP_GET_SUCCESS,employees);
    }
    /**
     * 功能描述:员工禁用、启用
     * @return : Result
     * @param employee
     */
    @PutMapping
    public Result updateEmp(@RequestBody Employee employee){
        int row=employeeService.updateEmp(employee);
        if(row>0){
            return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.EMP_UPDATE_SUCCESS);
        }
        return new Result(BusinessCodeConstant.EMP_UPDATE_ERROR,BusinessMessageConstant.EMP_UPDATE_ERROR);
    }
    /**
     * 功能描述:删除员工
     * @return : result
     * @param ids
     */
    @DeleteMapping("/empDel")
    public Result deleteEmp(@RequestParam Long[] ids){
        int row=employeeService.deleteEmp(ids);
        if (row>0){
            return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.EMP_DEL_SUCCESS);
        }
        return new Result(BusinessCodeConstant.EMP_DEL_ERROR,BusinessMessageConstant.EMP_DEL_ERROR);
    }
}
