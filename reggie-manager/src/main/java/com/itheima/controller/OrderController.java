package com.itheima.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.PageResult;
import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.entity.Orders;
import com.itheima.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * OrderController
 * 订单模块-控制器
 * @author lishuo
 * @date 2022/6/5
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrdersService ordersService;

    /**
     * 功能描述:分页查询
     * @param size
     * @param pageSize
     * @return : com.itheima.common.PageResult
     */
    @GetMapping("/page")
    public PageResult page(@RequestParam("page") Integer size,
                           @RequestParam("pageSize") Integer pageSize,
                           @RequestParam(required = false) Integer id,
                           @RequestParam(required = false)String beginTime,
                           @RequestParam(required = false)String endTime){
        //1.调用service执行分页查询
        Page<Orders> vo = ordersService.page(size,pageSize,id,beginTime,endTime);
            if(vo == null){
            return new PageResult(BusinessCodeConstant.LOGIN_ERROR,BusinessMessageConstant.PAGE_SELECT_ERROR);
            }
        //3.获取当前页列表数据、总条数
        List<Orders> records = vo.getRecords();
        long total = vo.getTotal();
        return new PageResult(BusinessCodeConstant.OK, BusinessMessageConstant.PAGE_SELECT_SUCCESS,records,total);
    }
    /**
     * 功能描述: 订单状态
     * @param orders
     * @return : com.itheima.common.Result
     */
   @PutMapping
    public Result update(@RequestBody Orders orders){
       //1.调用service,接收service业务处理结果
        int row = ordersService.update(orders);
        if(row < 1){
            return new Result(BusinessCodeConstant.LOGIN_ERROR,BusinessMessageConstant.ORDER_UPDATE_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.ORDER_UPDATE_SUCCESS);
   }


}
