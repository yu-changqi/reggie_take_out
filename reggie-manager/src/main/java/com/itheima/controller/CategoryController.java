package com.itheima.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.PageResult;
import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.entity.Category;
import com.itheima.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: GengZhiFei
 * @Desc: 分类管理模块 控制器
 * @Date: 2022/6/5 19:18
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    /**
     * 自动装配 CategoryService 分类管理模块 业务接口
     */
    @Autowired
    private CategoryService categoryService;

    /**
     * 模块功能 : 分页查询 控制器
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    private PageResult pageList(@RequestParam("page") Integer page, @RequestParam("pageSize") Integer pageSize) {
        //调用业务层 pageList 方法 进行分页查询
        Page<Category> date = categoryService.pageList(page, pageSize);
        //显示数据总条数
        long total = date.getTotal();
        //显示每页具体数据
        List<Category> records = date.getRecords();
        //封装分页结果
        if (records == null) {
            return new PageResult(BusinessCodeConstant.PAGE_SELECT_ERROR, BusinessMessageConstant.PAGE_SELECT_ERROR);
        }
        return new PageResult(BusinessCodeConstant.OK, BusinessMessageConstant.PAGE_SELECT_SUCCESS, records, total);
    }

    /**
     * 模块功能 : 新增 菜品 套餐 分类信息 控制器
     *
     * @param category
     * @return
     */
    @PostMapping
    private Result save(@RequestBody Category category) {
        //调用业务层 save 方法存储菜品种类信息
        int row = categoryService.save(category);
        //根据返回值 判断新增是否成功
        if (row < 1) {
            return new Result(BusinessCodeConstant.CATEGORY_CREATE_DISH_CLASSIFICATION_ERROR, BusinessMessageConstant.CATEGORY_CREATE_DISH_CLASSIFICATION_ERROR);
        }
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.CATEGORY_CREATE_DISH_CLASSIFICATION_OK);
    }

    /**
     * 模块功能 : 修改 菜品 套餐 分类信息 控制器
     *
     * @param category
     * @return
     */
    @PutMapping
    private Result upDate(@RequestBody Category category) {
        int row = categoryService.upDate(category);
        if (row < 1) {
            return new Result(BusinessCodeConstant.CATEGORY_UPDATE_CATEGORY_ERROR, BusinessMessageConstant.CATEGORY_UPDATE_CATEGORY_ERROR);
        }
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.CATEGORY_UPDATE_CATEGORY_OK);
    }

    /**
     * 模块功能 : (批量)删除 菜品 套餐 分类信息 控制器
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    private Result deleteByIds(@RequestParam("id") Long[] ids) {
        int row = categoryService.deleteByIds(ids);
        if (row < 1) {
            return new Result(BusinessCodeConstant.CATEGORY_DELETE_CATEGORY_ERROR, BusinessMessageConstant.CATEGORY_DELETE_CATEGORY_ERROR);
        }
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.CATEGORY_DELETE_CATEGORY_OK);
    }

    /**
     * 模块功能 : 根据分类类型(Type) 查询分类信息 控制器
     *
     * @param type
     * @return
     */
    @GetMapping("/list")
    private Result list(@RequestParam("type") Integer type) {
        List<Category> date = categoryService.list(type);
        if (date==null) {
            return new Result(BusinessCodeConstant.CATEGORY_SELECT_CATEGORY_BY_TYPE_ERROR, BusinessMessageConstant.CATEGORY_SELECT_CATEGORY_BY_TYPE_ERROR);
        }
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.CATEGORY_SELECT_CATEGORY_BY_TYPE_OK,date);
    }
}
