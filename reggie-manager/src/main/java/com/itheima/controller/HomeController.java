package com.itheima.controller;

import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.entity.HomeImage;
import com.itheima.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Author: Eleike
 * @Desc: 首页模块处理器
 * @Date: 2022/6/6 17:45
 */
@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private HomeService homeService;

    /**
     * 方法描述 获取菜品套餐数量处理器
     * @since: 1.14.1
     * @param:
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/6
     */
    @GetMapping("/proportion")
    public Result getDishAndSetMealCount(){
        List<Map> proportionCount = homeService.getDishAndSetMealCount();
        if (proportionCount == null){
            return new Result(BusinessCodeConstant.HOME_SELECT_DISH_AND_SETMEAL_COUNT_ERROR, BusinessMessageConstant.HOME_SELECT_DISH_AND_SETMEAL_COUNT_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.HOME_SELECT_DISH_AND_SETMEAL_COUNT_SUCCESS,proportionCount);
    }

    /**
     * 方法描述 获取今七天新增用户数量
     * @since: 1.14.1
     * @param: dateData
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/7
     */
    @GetMapping("/userCount")
    public Result getNewUserOfWeek(String[] dateData){
        List<Integer> userCount = homeService.getNewUserOfWeek(Arrays.asList(dateData));
        if (userCount == null){
            return new Result(BusinessCodeConstant.HOME_SELECT_NEW_USER_COUNT_ERROR,BusinessMessageConstant.HOME_SELECT_NEW_USER_COUNT_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.HOME_SELECT_NEW_USER_COUNT_SUCCESS,userCount);
    }

    /**
     * 方法描述 获取首页轮播图图片
     * @since: 1.14.1
     * @param:
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/7
     */
    @GetMapping("/image")
    public Result getHomeImage(){
        List<HomeImage> homeImageList = homeService.getHomeImage();
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.HOME_SELECT_HOME_IMAGE_SUCCESS,homeImageList);
    }

    /**
     * 方法描述 修改首页轮播图图片
     * @since: 1.14.1
     * @param: homeImages
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/7
     */
    @PostMapping("/image")
    public Result updateOrAddHomeImageById(@RequestBody List<HomeImage> homeImages) throws IllegalAccessException {
        int rows = homeService.updateOrAddHomeImageById(homeImages);
        if (rows < 1){
            return new Result(BusinessCodeConstant.HOME_UPDATE_HOME_IMAGE_ERROR,BusinessMessageConstant.HOME_UPDATE_HOME_IMAGE_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.HOME_UPDATE_HOME_IMAGE_SUCCESS,rows);
    }
}
