package com.itheima.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.PageResult;
import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.RecycleMessageConstant;
import com.itheima.dto.RecycleDTO;
import com.itheima.entity.Dish;
import com.itheima.service.RecycleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: qiChangYue
 * @Desc:回收站管理控制器
 * @Date: 2022/6/6 11:23
 */
@RestController
@RequestMapping("/recycle")
public class RecycleController {
    @Autowired
    private RecycleService recycleService;

    /**
     * 功能描述 :查询被删除的菜品
     * @param recycleDTO
     * @return : com.itheima.common.PageReesult
     */
    @PostMapping("/tombstone")
    public PageResult findByListRecycle(@RequestBody RecycleDTO recycleDTO) {
        Page<Dish> dishPage = recycleService.findByListRecycle(recycleDTO);
        if (dishPage == null){
            return new PageResult(BusinessCodeConstant.RECYCLE_SELECT_ERROR,RecycleMessageConstant.DISH_GET_ERRO);
        }
        //获取当前页列表数据
        List<Dish> records = dishPage.getRecords();
        // 获取总条数
        long total = dishPage.getTotal();
        return new PageResult(BusinessCodeConstant.OK, RecycleMessageConstant.DISH_GET_SUCCESS, records, total);
    }

    /**
     * 功能描述 : 菜品回收站还原功能
     * @param ids
     * @return :
     */
    @GetMapping("/restore")
    public Result recycleBinRestore(@RequestParam Long[] ids){
        int row=recycleService.recycleBinRestore(ids);
        if (row<1){
            return new Result(BusinessCodeConstant.RECYCLE_REDUCTION_ERROR,RecycleMessageConstant.DISH_RESTORE_ERRO);
        }
        return new Result(BusinessCodeConstant.OK,RecycleMessageConstant.DISH_RESTORE_SUCCESS);
    }

    /**
     * 功能描述 : 菜品回收站删除
     * @param ids
     * @return :
     */
    @DeleteMapping("/romove")
    public PageResult recycleBinRomove(@RequestParam("ids") Long[] ids){
        int row=recycleService.recycleBinRomove(ids);
        if (row<1){
            return new PageResult(BusinessCodeConstant.RECYCLE_DELETE_ERROR,RecycleMessageConstant.DISH_ROMOVE_ERRO);
        }
        return new PageResult(BusinessCodeConstant.OK,RecycleMessageConstant.DISH_ROMOVE_SUCCESS);
    }
}
