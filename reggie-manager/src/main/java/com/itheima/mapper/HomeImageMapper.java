package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.HomeImage;

/**
 * @Author: Eleike
 * @Desc: 首页轮播图mapper
 * @Date: 2022/6/7 17:05
 */
public interface HomeImageMapper extends BaseMapper<HomeImage> {
}
