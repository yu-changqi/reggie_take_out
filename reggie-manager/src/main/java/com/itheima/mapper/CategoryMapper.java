package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Category;
/**
 * @Author: GengZhiFei
 * @Desc: 分类管理模块 数据层接口
 * @Date: 2022/6/5 19:16
 */
public interface CategoryMapper extends BaseMapper<Category> {
}
