package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.User;

/**
 * @Author: Eleike
 * @Desc: app端用户实体类Mapper
 * @Date: 2022/6/7 14:29
 */
public interface UserMapper extends BaseMapper<User> {
}
