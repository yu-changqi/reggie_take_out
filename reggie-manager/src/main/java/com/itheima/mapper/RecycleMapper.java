package com.itheima.mapper;

import com.itheima.dto.RecycleDTO;
import com.itheima.entity.Dish;

import java.util.List;

public interface RecycleMapper {
    /**
     *查询删除菜品
     */
    List<Dish> selectDeleteDishList(RecycleDTO recycleDTO);
    /**
     *查询删除菜品个数
     */
    Integer selectDeleteDishCount();
    /**
     *菜品恢复
     */
    int updateDishIsReduction(Long[] ids);
    /**
     *套餐恢复
     */
    int updateSetmealDishIsReduction(Long[] ids);
    /**
     *菜品删除
     */
    int deletedDishIsReduction(List<Long> asList);
    /**
     *t套餐删除
     */
    int deletedSetmealDishIsReduction(List<Long> asLists);
}
