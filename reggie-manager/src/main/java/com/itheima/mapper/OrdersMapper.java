package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Orders;
/**
 * OrdersMapper
 *  订单详情-mapper接口
 * @author lishuo
 * @date 2022/6/7
 */
public interface OrdersMapper extends BaseMapper<Orders> {
}
