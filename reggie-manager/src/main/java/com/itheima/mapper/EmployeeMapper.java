package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: liuxinwei
 * @Desc:  用户登录-mapper接口
 * @Date: 2022/6/5 16:57
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

}
