package com.itheima.service;

import com.itheima.entity.HomeImage;

import java.util.List;
import java.util.Map;

/**
 * @Author: Eleike
 * @Desc: 首页模块-service接口
 * @Date: 2022/6/6 17:44
 */
public interface HomeService {
    /**
     * 方法描述 获取菜品和套餐各自数量
     * @since: 1.14.1
     * @param: null
     * @return: List<Map>
     * @author: Eleike
     * @date: 2022/6/6
     */
    List<Map> getDishAndSetMealCount();

    /**
     * 方法描述 获取近七天新增的app端用户
     * @since: 1.14.1
     * @param:
     * @return: list<Integer>
     * @author: Eleike
     * @date: 2022/6/7
     */
    List<Integer> getNewUserOfWeek(List<String> dateData);

    /**
     * 方法描述 获取首页轮播图信息
     * @since: 1.14.1
     * @param:
     * @return: List<HomeImage>
     * @author: Eleike
     * @date: 2022/6/7
     */
    List<HomeImage> getHomeImage();

    /**
     * 方法描述 修改首页轮播图图片
     * @since: 1.14.1
     * @param: homeImages
     * @return: int
     * @author: Eleike
     * @date: 2022/6/7
     */
    int updateOrAddHomeImageById(List<HomeImage> homeImages) throws IllegalAccessException;
}
