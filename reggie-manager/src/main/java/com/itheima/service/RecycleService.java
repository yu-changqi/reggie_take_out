package com.itheima.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.dto.RecycleDTO;
import com.itheima.entity.Dish;

public interface RecycleService {


    Page<Dish> findByListRecycle(RecycleDTO recycleDTO);

    

    int recycleBinRestore(Long[] ids);

    int recycleBinRomove(Long[] ids);
}
