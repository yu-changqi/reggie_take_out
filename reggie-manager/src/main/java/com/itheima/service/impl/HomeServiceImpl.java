package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.entity.HomeImage;
import com.itheima.entity.User;
import com.itheima.mapper.DishMapper;
import com.itheima.mapper.HomeImageMapper;
import com.itheima.mapper.SetmealMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.service.HomeService;
import com.itheima.util.StringTrimUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Eleike
 * @Desc: 首页模块-service实现
 * @Date: 2022/6/6 17:44
 */
@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private HomeImageMapper homeImageMapper;

    @Override
    public List<Map> getDishAndSetMealCount() {
        List<Map> list = null;
        //分别查询菜品和套餐的数量
        Integer dishCount = dishMapper.selectCount(null);
        Integer setMealCount = setmealMapper.selectCount(null);
        //如果两者都为空则查询失败
        if (setMealCount == null && dishCount == null){
            return list;
        }
        if (dishCount < 0){
            dishCount = 0;
        }
        if (setMealCount < 0){
            setMealCount = 0;
        }
        //结果封装到map中
        HashMap<Object, Object> dishMap = new HashMap<>(2);
        dishMap.put("name","菜品");
        dishMap.put("value",dishCount);
        HashMap<Object, Object> setMealMap = new HashMap<>(2);
        setMealMap.put("name","套餐");
        setMealMap.put("value",setMealCount);
        list = new ArrayList<>();
        list.add(dishMap);
        list.add(setMealMap);
        return list;
    }

    @Override
    public List<Integer> getNewUserOfWeek(List<String> dateData) {
        List<Integer> userCount = null;
        if (dateData == null){
            return userCount;
        }
        int count = 0;
        userCount = new ArrayList<>();
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        for (String dateDatum : dateData) {
            String[] split = dateDatum.trim().split("/");
            LocalDate date = LocalDate.of(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
            queryWrapper.clear();
            queryWrapper.likeRight(User::getCreateTime,date + " ");
            count = userMapper.selectCount(queryWrapper);
            userCount.add(count);
        }
        return userCount;
    }

    @Override
    public List<HomeImage> getHomeImage() {
        LambdaQueryWrapper<HomeImage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(HomeImage::getUpdateTime);
        List<HomeImage> homeImages = homeImageMapper.selectList(null);
        //数量大于三只要前三张图片
        if (homeImages.size() > 3){
            ArrayList<HomeImage> homeImageList = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                homeImageList.add(homeImages.get(i));
            }
            return homeImageList;
        }
        return homeImages;
    }

    @Override
    public int updateOrAddHomeImageById(List<HomeImage> homeImages) throws IllegalAccessException {
        int rows = 0;
        if (homeImages == null){
            return rows;
        }
        for (HomeImage homeImage : homeImages) {
            //去除两端空格
            homeImage = StringTrimUtil.BeanStringAttributeTrimUtil(homeImage);
            //没有id则添加，否则更改
            if(homeImage.getId() == null){
                rows += homeImageMapper.insert(homeImage);
            }else {
                rows += homeImageMapper.updateById(homeImage);
            }

        }
        return rows;
    }
}
