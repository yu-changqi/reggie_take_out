package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.entity.Orders;
import com.itheima.mapper.OrdersMapper;
import com.itheima.service.OrdersService;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * OrderSerivesImpl
 *  订单功能实现
 * @author lishuo
 * @date 2022/6/7
 */
@Service
public class OrderSerivesImpl implements OrdersService {

    @Autowired
    private OrdersMapper ordersMapper;
    @Override
    public Page<Orders> page(Integer size, Integer pageSize,Integer id,String beginTime,String endTime) {
        //校检参数
        if(size < 1){
            size = 1;
        }
        if(pageSize < 1){
            pageSize = 10;
        }
        //查询订单列表
        Page<Orders> ordersPage = new Page<>(size,pageSize);
        LambdaQueryWrapper<Orders> wrapper = new LambdaQueryWrapper<>();
        //订单号不为空,设置查询条件
        if(id != null){
            wrapper.like(Orders::getId,id);
        }
        //如果开始时间和结束时间不为空,并且开始时间不等于结束时间
        if(beginTime != null && endTime != null && beginTime != endTime){
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime begin = LocalDateTime.parse(beginTime, dateTimeFormatter);
            LocalDateTime end = LocalDateTime.parse(endTime, dateTimeFormatter);
            wrapper.between(Orders::getOrderTime,begin,end);
        }
        Page<Orders> ordersPageMapper = ordersMapper.selectPage(ordersPage, wrapper);
        return  ordersPageMapper;
    }
    @Override
    public int update(Orders orders) {
        int row = 0;
        //校验参数
        Integer status = orders.getStatus();
        Long id = orders.getId();
        if (status == 3){
            LambdaUpdateWrapper<Orders> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(Orders::getId,id);
            updateWrapper.set(Orders::getStatus,status);
            row = ordersMapper.update(null, updateWrapper);
        }
        if(status == 4){
            LambdaUpdateWrapper<Orders> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(Orders::getId,id);
            updateWrapper.set(Orders::getStatus,status);
            row = ordersMapper.update(null, updateWrapper);
        }
        if(status == 5){
            LambdaQueryWrapper<Orders> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Orders::getId,id);
            row = ordersMapper.delete(wrapper);
        }
        return  row;
    }
}
