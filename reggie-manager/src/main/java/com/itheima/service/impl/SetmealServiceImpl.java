package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.PageResult;
import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.constant.CommonConstant;
import com.itheima.dto.SetmealDTO;
import com.itheima.entity.Setmeal;
import com.itheima.entity.SetmealDish;
import com.itheima.exception.HeimaExceptionThrow;
import com.itheima.mapper.SetmealDishMapper;
import com.itheima.mapper.SetmealMapper;
import com.itheima.service.SetmealService;
import com.itheima.vo.SetmealVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: jingHaoHao
 * @Desc:套餐管理系统--Service层实现
 * @Date: 2022/6/6 11:02
 */
@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;

    @Override
    public PageResult pagingQuery(Integer page, Integer pageSize, String name) {
        //1.检查参数
        if (page < 1) {
            page = 1;
        }
        if (pageSize < 1) {
            pageSize = 10;
        }
        //2.封装查询条件
        Page<Setmeal> setmealPage = new Page<>();
        LambdaQueryWrapper<Setmeal> wrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(name)) {
            wrapper.like(Setmeal::getName, name);
        }
        //3.封装结果并返回
        Page<Setmeal> resultPage = setmealMapper.selectPage(setmealPage, wrapper);
        List<Setmeal> records = resultPage.getRecords();
        long total = resultPage.getTotal();

        return new PageResult(BusinessCodeConstant.OK, BusinessMessageConstant.PAGE_SELECT_SUCCESS, records, total);
    }

    @Override
    @Transactional
    public Result addSetmeal(SetmealDTO setmealDTO) {
        //1.校验，业务异常判断
        if (setmealDTO == null) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_DATA_NO_UPLOAD, BusinessMessageConstant.SETMEAL_DATA_NO_UPLOAD);
        }
        String name = setmealDTO.getName();
        LambdaQueryWrapper<Setmeal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Setmeal::getName, name);
        List<Setmeal> setmeals = setmealMapper.selectList(wrapper);
        if (setmeals != null && setmeals.size() != 0) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_DATA_PACKAGE, BusinessMessageConstant.SETMEAL_DATA_PACKAGE);
        }
        //进行添加数据，并添加到数据库
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO, setmeal);
        if (setmeal == null) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_DATA_ADD_ERROR, BusinessMessageConstant.SETMEAL_DATA_ADD_ERROR);
        }
        setmealMapper.insert(setmeal);
        //补全数据,并添加到数据库
        List<SetmealDish> setmealDishList = setmealDTO.getSetmealDishes();
        int row = 0;
        for (SetmealDish setmealDish : setmealDishList) {
            setmealDish.setSetmealId(setmeal.getId());
            int insert = setmealDishMapper.insert(setmealDish);
            row += insert;
        }
        if (row < 1) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_ADD_ERROR, BusinessMessageConstant.SETMEAL_DATA_ADD_ERROR);
        }
        return  new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SETMEAL_DATA_ADD_SUCCESS);
    }

    @Override
    @Transactional
    public Result dataEcho(Long id) {
        //1.校验，业务异常判断
        if (id == null) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_DATA_NO_UPLOAD, BusinessMessageConstant.SETMEAL_DATA_NO_UPLOAD);
        }
        //进行数据回显
        SetmealVO setmealVO = new SetmealVO();
        //查寻套餐表数据,将数据复制到VO视图对象里面
        LambdaQueryWrapper<Setmeal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Setmeal::getId, id);
        Setmeal setmeal = setmealMapper.selectOne(wrapper);
        BeanUtils.copyProperties(setmeal, setmealVO);
        //查询套菜品关联表将数据放入到VO对象中
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> setmealDishes = setmealDishMapper.selectList(queryWrapper);
        setmealVO.setSetmealDishes(setmealDishes);
        if (setmealVO == null) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_SELECT_ERROR, BusinessMessageConstant.SETMEAL_SELECT_ERROR);
        }
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SETMEAL_SELECT_SUCCESS, setmealVO);
    }

    @Override
    @Transactional
    public Result update(SetmealDTO setmealDTO) {
        int row=0;
        //1.校验，业务异常判断
        if (setmealDTO == null) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_DATA_NO_UPLOAD, BusinessMessageConstant.SETMEAL_DATA_NO_UPLOAD);
        }
        //判断用户数据是否是否重复
        String name = setmealDTO.getName();
        Long id = setmealDTO.getId();
        LambdaQueryWrapper<Setmeal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Setmeal::getName, name);
        Setmeal setmeal = setmealMapper.selectOne(wrapper);
        //根据查询的套餐进行判断,数据是否重复
        if(setmeal!=null){
        if (name.equals(setmeal.getName())&&!id.equals(setmeal.getId())) {
                HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_DATA_PACKAGE, BusinessMessageConstant.SETMEAL_DATA_PACKAGE);
        }
        }
        //执行业务逻辑
        //修改套餐表内数据
        Setmeal updateObject = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,updateObject);
        row = setmealMapper.updateById(updateObject);
        if(row<1){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_PUT_ERROR,BusinessMessageConstant.SETMEAL_PUT_ERROR);
        }
        //根据用户套餐ID先将菜品进行删除,然后在将新的数据存入关联表
        //删除原来数据
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,id);
        row = setmealDishMapper.delete(queryWrapper);
        if (row<1){
                HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_PUT_ERROR,BusinessMessageConstant.SETMEAL_PUT_ERROR);
        }
        //添加新的数据
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        if (setmealDishes!=null){
            for (SetmealDish setmealDish : setmealDishes) {
                setmealDish.setSetmealId(id);
                setmealDishMapper.insert(setmealDish);
            }
        }

        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.SETMEAL_PUT_SUCCESS);
    }

    @Override
    public Result deleteByIds(Long[] ids) {
        Result result = null;
        //1.校验，业务异常判断
        if (ids == null || ids.length == 0) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_DATA_NO_UPLOAD, BusinessMessageConstant.SETMEAL_DATA_NO_UPLOAD);
        }
        //2.业务逻辑实现
        LambdaUpdateWrapper<Setmeal> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(Setmeal::getIsDeleted, CommonConstant.LOGIC_DELETE);
        wrapper.in(Setmeal::getId, ids);
        //3.获取数据
        int row = setmealMapper.update(new Setmeal(), wrapper);
        //4.进行数据判断
        if (row < 1) {
            return result = new Result(BusinessCodeConstant.SETMEAL_DELETE_ERROR, BusinessMessageConstant.SETMEAL_DELETE_ERROR);
        }
        return result = new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SETMEAL_DELETE_SUCCESS);
    }

    @Override
    @Transactional
    public Result findSetmealListByCategoryId(Integer status, Long[] ids) {
        Result result = null;
        //1.校验，业务异常判断
        if (ids == null || ids.length == 0) {
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.SETMEAL_DATA_NO_UPLOAD, BusinessMessageConstant.SETMEAL_DATA_NO_UPLOAD);
        }
        //2.业务逻辑实现
        LambdaUpdateWrapper<Setmeal> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(Setmeal::getStatus, status);
        wrapper.in(Setmeal::getId, ids);
        //获取数据影响的行数，对行数进行判断
        int row = setmealMapper.update(new Setmeal(), wrapper);
        if (row < 1) {
            return result = new Result(BusinessCodeConstant.SETMEAL_POST_SATE_ERROR, BusinessMessageConstant.SETMEAL_POST_STATE_ERROR);
        }
        return result = new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SETMEAL_POST_STATE_SUCCESS);
    }


}
