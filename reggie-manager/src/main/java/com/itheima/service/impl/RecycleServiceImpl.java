package com.itheima.service.Impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.dto.RecycleDTO;
import com.itheima.entity.Dish;
import com.itheima.mapper.DishMapper;
import com.itheima.mapper.RecycleMapper;
import com.itheima.service.RecycleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: qiChangYue
 * @Desc:回收站管理业务实现
 * @Date: 2022/6/6 12:29
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class RecycleServiceImpl implements RecycleService {

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private RecycleMapper recycleMapper;
    /**
     * 功能描述 : 回收站页面回显--查询被删除的菜品
     * @param
     * @return :
     */
    @Override
    public Page<Dish> findByListRecycle(RecycleDTO recycleDTO) {
        Page<Dish> pageResult=null;
        Integer page = recycleDTO.getPage();
        Integer size = recycleDTO.getSize();
        LocalDateTime[] dateRange = recycleDTO.getDateRange();
        //分页条件
        if (page < 1){
            page = 1;
        }
        if (size < 1){
            size = 10;
            recycleDTO.setSize(size);
        }
        recycleDTO.setPage((page - 1) * size);
        if (dateRange != null && dateRange.length > 1){
            recycleDTO.setStartTime(dateRange[0]);
            recycleDTO.setEndTime(dateRange[1]);
        }
        if (StringUtils.isNotBlank(recycleDTO.getName())){
            recycleDTO.setName(recycleDTO.getName().trim());
        }
        List<Dish> dishList = recycleMapper.selectDeleteDishList(recycleDTO);
        if (dishList == null){
            return pageResult;
        }
        Integer count = recycleMapper.selectDeleteDishCount();
        pageResult = new Page<>();
        pageResult.setRecords(dishList);
        pageResult.setTotal(count);
        return pageResult;
    }

    /**
     * 功能描述 : 菜品回收站--还原
     * @param ids
     * @return :
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int recycleBinRestore(Long[] ids) {
        int rows=0;
        if (ids == null){
            return rows;
        }
        rows = recycleMapper.updateDishIsReduction(ids);
        if (rows < 1){
            return rows;
        }
        recycleMapper.updateSetmealDishIsReduction(ids);
        return rows;
    }

    /**
     * 功能描述 : 菜品回收站--删除.物理删除
     * @param ids
     * @return :
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int recycleBinRomove(Long[] ids) {
        int rows=0;
        if (ids == null){
            return rows;
        }
        //删除   根据id批量删除
        rows = recycleMapper.deletedDishIsReduction(Arrays.asList(ids));
        if (rows < 1){
            return rows;
        }
        recycleMapper.deletedSetmealDishIsReduction(Arrays.asList(ids));
        return rows;
    }

}
