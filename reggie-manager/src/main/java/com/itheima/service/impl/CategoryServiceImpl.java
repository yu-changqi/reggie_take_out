package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.constant.SystemMessageConstant;
import com.itheima.entity.Category;
import com.itheima.mapper.CategoryMapper;
import com.itheima.service.CategoryService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @Author: GengZhiFei
 * @Desc: 分类管理模块 业务层
 * @Date: 2022/6/5 19:17
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    /**
     * 自动装配 CategoryMapper 分类管理模块 数据层接口
     */
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public Page<Category> pageList(Integer page, Integer pageSize) {
        //判断 并 初始化 当前页
        if (page < 1) {
            page = 1;
        }
        //判断 并 初始化 每页多少条
        if (pageSize < 1) {
            pageSize = 10;
        }
        //封装分页查询条件
        Page<Category> categoryPage = new Page<>(page, pageSize);
        //调用 categoryMapper 接口, 进行分页查询
        LambdaQueryWrapper<Category> categoryWrapper = new LambdaQueryWrapper<>();
        categoryWrapper.orderByDesc(Category::getUpdateTime);
        Page<Category> pageDate = categoryMapper.selectPage(categoryPage, categoryWrapper);
        //返回查询结果
        return pageDate;
    }

    @Override
    public int save(Category category) {
        int row = 0;
        //判断传入对象是否为空
        if (category == null) {
            return row;
        }
        //取出菜品种类信息的 名字 和 顺序
        String name = category.getName();
        Integer sort = category.getSort();
        //判断 名字 和 顺序 是否为空, 为空返回 row=0
        if (StringUtils.isBlank(name)) {
            return row;
        }
        if (sort < 1) {
            return row;
        }
        //查询 数据库 菜品种类表, 判断是否有名字相同的信息, 查到信息, 说明待存储信息重复, 返回row=0
        LambdaQueryWrapper<Category> categoryWrapperByName = new LambdaQueryWrapper<>();
        categoryWrapperByName.eq(Category::getName, name);
        Category categoryByName = categoryMapper.selectOne(categoryWrapperByName);
        if (categoryByName != null) {
            return row;
        }
        //查询 数据库 菜品种类表, 判断是否有排序相同的信息, 查到信息, 说明待存储信息重复, 返回row=0
        LambdaQueryWrapper<Category> categoryWrapperBySort = new LambdaQueryWrapper<>();
        categoryWrapperBySort.eq(Category::getSort, sort);
        Category categoryBySort = categoryMapper.selectOne(categoryWrapperBySort);
        if (categoryBySort != null) {
            return row;
        }
        //调用 categoryMapper 接口, 新增菜品分类信息
        row = categoryMapper.insert(category);
        return row;
    }

    @Override
    public int upDate(Category category) {
        int row = 0;
        //判断传入对象是否为空
        if (category == null) {
            return row;
        }
        //取出菜品种类信息的 名字 和 顺序, 判断 名字 是否为空, 为空返回 row=0
        String name = category.getName();
        if (StringUtils.isBlank(name)) {
            return row;
        }
        //查询 数据库 菜品种类表, 判断是否有名字相同的信息, 查到信息, 说明待存储信息重复, 返回row=0
        LambdaUpdateWrapper<Category> categoryWrapperByName = new LambdaUpdateWrapper<>();
        categoryWrapperByName.eq(Category::getName, name);
        Category categoryByName = categoryMapper.selectOne(categoryWrapperByName);
        //如果查到了 并且 传参名称和数据库查询到的信息名称相同的话, 说明改名重复, 不通过
        if (categoryByName != null && !category.getId().equals(categoryByName.getId())) {
            throw new RuntimeException(SystemMessageConstant.CATEGORY_NAME_IS_EXIST);
        }
        LambdaUpdateWrapper<Category> categoryWrapperByName1 = new LambdaUpdateWrapper<>();
        categoryWrapperByName1.eq(Category::getSort,category.getSort());
        Category categoryByName1 = categoryMapper.selectOne(categoryWrapperByName1);
        //如果查到了 并且 传参名称和数据库查询到的信息名称相同的话, 说明排序重复, 不通过
        if (categoryByName1 != null && !category.getId().equals(categoryByName1.getId())) {
            throw new RuntimeException(SystemMessageConstant.CATEGORY_NAME_IS_EXIST);
        }

        //调用 categoryMapper 接口, 修改分类信息
        row = categoryMapper.updateById(category);
        return row;
    }

    @Override
    public int deleteByIds(Long[] ids) {
        //判断 ids 非空
        int row = 0;
        if (ids == null) {
            return row;
        }
        //调用 categoryMapper 接口, (批量)删除分类数据
        row = categoryMapper.deleteBatchIds(Arrays.asList(ids));
        return row;
    }

    @Override
    public List<Category> list(Integer type) {
        //判断 type 非空
        if (type==null){
            return null;
        }
        //调用 categoryMapper 接口, 根据分类类型(Type) 查询分类信息
        LambdaQueryWrapper<Category> categoryWrapper = new LambdaQueryWrapper<>();
        categoryWrapper.eq(Category::getType,type);
        List<Category> categories = categoryMapper.selectList(categoryWrapper);
        return categories;
    }
}
