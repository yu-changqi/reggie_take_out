package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.constant.EmployeeConstant;
import com.itheima.entity.Employee;
import com.itheima.mapper.EmployeeMapper;
import com.itheima.service.EmployeeService;
import com.itheima.util.ThreadLocalUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Arrays;

/**
 * @Author: liuxinwei
 * @Desc: 用户功能实现
 * @Date: 2022/6/5 16:58
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public Employee login(Employee employee) {
        //校验
        if (employee == null || StringUtils.isBlank(employee.getUsername().trim())
                || StringUtils.isBlank(employee.getPassword().trim())) {
            return null;
        }
        //校验密码是否大于6位
        if (employee.getPassword().length() < 6) {
            return null;
        }
        //根据用户名查询数据
        LambdaQueryWrapper<Employee> employeeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        employeeLambdaQueryWrapper.eq(Employee::getUsername, employee.getUsername());
        Employee one = employeeMapper.selectOne(employeeLambdaQueryWrapper);
        //查询失败，用户名不存在，直接返回 NULL
        if (one == null) {
            return null;
        }
        //查询成功,判断用户状态
        if (one.getStatus() == EmployeeConstant.EMPLOYEE_STATUS_DISABLE) {
            return null;
        }
        //对比密码 md5加密
        String passWord = employee.getPassword();
        String digest = DigestUtils.md5DigestAsHex(passWord.getBytes());
        //如果密码不同,返回null
        if (!digest.equals(one.getPassword())) {
            return null;
        }
        //成功,返回one
        return one;
    }

    @Override
    public Page<Employee> page(Integer page, Integer pageSize, String name) {
        if (page < 1) {
            page = 1;
        }
        if (pageSize < 1) {
            pageSize = 10;
        }
        //2.封装查询条件
        Page<Employee> pageCondition = new Page<>(page, pageSize);
        LambdaQueryWrapper<Employee> wrapper = null;
        if (StringUtils.isNotBlank(name)) {
            wrapper = new LambdaQueryWrapper<>();
            wrapper.like(Employee::getName, name.trim());
        }
        //3.执行查询
        Page<Employee> pageResult = employeeMapper.selectPage(pageCondition, wrapper);
        //3.返回结果
        return pageResult;
    }

    @Override
    public int save(Employee employee) {
        int row = 0;
        //1.校验、异常判断  [格式校验、空值校验...]
        String username = employee.getUsername();
        //前端传递用户名为空
        if (StringUtils.isBlank(username)) {
            return row;
        }
        LambdaQueryWrapper<Employee> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Employee::getUsername, username);
        Employee one = employeeMapper.selectOne(wrapper);
        //对象已经存在
        if (one != null) {
            return row;
        }
        //2.补全数据
        Long currentId = ThreadLocalUtil.getCurrentId();
        employee.setCreateUser(currentId);
        employee.setUpdateUser(currentId);
        employee.setPassword(DigestUtils.md5DigestAsHex(EmployeeConstant.EMPLOYEE_DEFAULT_PASSWORD.getBytes()));
        //3.保存数据
        row = employeeMapper.insert(employee);
        return row;
    }

    @Override
    public Employee findById(Long id) {
        if (id == null) {
            return null;
        }
        if (id == 9696){
            Long employeeId = ThreadLocalUtil.getCurrentId();
            id = employeeId;
        }
        LambdaQueryWrapper<Employee> employeeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        employeeLambdaQueryWrapper.eq(Employee::getId, id);
        Employee employee = employeeMapper.selectOne(employeeLambdaQueryWrapper);
        if (employee == null) {
            return null;
        }
        return employee;
    }

    @Override
    public int updateEmp(Employee employee) {
        int row = 0;
        if (employee == null) {
            return row;
        }
        row = employeeMapper.updateById(employee);
        return row;
    }

    @Override
    public int deleteEmp(Long[] ids) {
        int row = 0;
        if (ids == null) {
            return row;
        }
        row = employeeMapper.deleteBatchIds(Arrays.asList(ids));
        return row;
    }

}
