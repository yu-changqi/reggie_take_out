package com.itheima.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.entity.Employee;
import org.springframework.stereotype.Service;

/**
 * @Author: liuxinwei
 * @Desc: 用户-服务接口
 * @Date: 2022/6/5 15:05
 */
@Service
public interface EmployeeService {
    /**
     * 功能描述: 登录功能实现
     *
     * @param employee
     * @return :
     */
    Employee login(Employee employee);

    /**
     * 功能描述:分页查询员工信息
     * @return : Page<Employee>
     * @param page,pageSize,name
     */
    Page<Employee> page(Integer page, Integer pageSize, String name);

    /**
     * 功能描述:新增员工
     * @return :int
     * @param employee
     */
    int save(Employee employee);
    /**
     * 功能描述:员工信息回显
     * @return : employee
     * @param id
     */
    Employee findById(Long id);
    /**
     * 功能描述:员工禁用,启用
     * @return : int
     * @param employee
     */
    int updateEmp(Employee employee);
    /**
     * 功能描述:删除员工
     * @return : int
     * @param ids
     */
    int deleteEmp(Long[] ids);
}
