package com.itheima.service;

import com.itheima.common.PageResult;
import com.itheima.common.Result;
import com.itheima.dto.SetmealDTO;

/**
 * @Author: jingHaoHao
 * @Desc:套餐管理系统--Service层
 * @Date: 2022/6/6 11:02
 */
public interface SetmealService {

    /**
     * 功能描述  : 套餐模块-分页查询--功能实现
     * @param page
     * @return :com.itheima.common.Result
     */
    PageResult pagingQuery(Integer page, Integer pageSize,String name);
    /**
     * 功能描述  :套餐模块-逻辑删除--功能实现
     * @param ids
     * @return :
     */
    Result deleteByIds(Long[] ids);
    /**
     * 功能描述  :套餐模块-套餐状态修改--功能实现
     * @param status
     * @param ids
     * @return : com.itheima.common.Result
     */
    Result findSetmealListByCategoryId(Integer status, Long[] ids);

    /**
     * 功能描述  :套餐模块-添加套餐--功能实现
     * @param setmealDTO
     * @return :com.itheima.common.Result
     */
    Result addSetmeal(SetmealDTO setmealDTO);
    /**
     * 功能描述  :套餐管理-数据回显-业务实现
     * @param id
     * @return : dataEcho
     */
    Result dataEcho(Long id);
    /**
     * 功能描述  : 套餐管理-套餐修改-业务实现
     * @param setmealDTO
     * @return : com.itheima.common.Result
     */
    Result update(SetmealDTO setmealDTO);
}
