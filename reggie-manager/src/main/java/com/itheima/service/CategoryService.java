package com.itheima.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.entity.Category;

import java.util.List;

/**
 * @Author: GengZhiFei
 * @Desc: 分类管理模块 业务接口
 * @Date: 2022/6/5 19:16
 */
public interface CategoryService {

    /**
     * 模块功能 : 分页查询
     * @param page
     * @param pageSize
     * @return
     */
    Page<Category> pageList(Integer page, Integer pageSize);

    /**
     * 模块功能 : 新增 菜品 套餐 分类信息
     * @param category
     * @return
     */
    int save(Category category);

    /**
     * 模块功能 : 修改 菜品 套餐 分类信息
     * @param category
     * @return
     */
    int upDate(Category category);

    /**
     * 模块功能 : (批量)删除 菜品 套餐 分类信息
     * @param ids
     * @return
     */
    int deleteByIds(Long[] ids);

    /**
     * 模块功能 : 根据分类类型(Type) 查询分类信息
     * @param type
     * @return
     */
    List<Category> list(Integer type);

}
