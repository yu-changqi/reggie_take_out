package com.itheima.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.entity.Orders;

/**
 * OrderService
 * 订单模块-业务接口
 * @author lishuo
 * @date 2022/6/5
 */
public interface OrdersService {
    /**
     *  分页查询订单
     */
    Page<Orders> page(Integer size, Integer pageSize,Integer id,String beginTime,String endTime);
    /**
     *  修改订单状态
     */
    int update(Orders orders);


}
