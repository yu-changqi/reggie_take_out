package com.itheima;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @Author: zhuan
 * @Desc: 瑞吉点餐-后端管理系统
 * @Date: 2022-06-04 22:05:22
 */
@Slf4j
@EnableWebMvc
@SpringBootApplication
@MapperScan("com.itheima.mapper")
@ServletComponentScan("com.itheima.filter")
public class ReggieManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReggieManagerApplication.class, args);
        log.info("项目启动成功！");
    }
}
