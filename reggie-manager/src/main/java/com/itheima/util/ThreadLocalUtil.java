package com.itheima.util;

/**
 * @Author: Eleike
 * @Desc: 线程变量副本数据(ThreadLocal)存取工具类
 * @Date: 2022/6/5 10:43
 */
public class ThreadLocalUtil {

    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 设置内容
     * @param id
     */
    public static void setCurrentId(Long id){
        threadLocal.set(id);
    }

    /**
     * 功能描述: 获取值
     */
    public static Long getCurrentId(){
        return threadLocal.get();
    }

    /**
     * 功能描述: 资源释放，清除资源
     */
    public static void remove(){
        threadLocal.remove();
    }
}
