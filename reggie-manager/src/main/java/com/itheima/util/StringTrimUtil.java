package com.itheima.util;

import java.lang.reflect.Field;

/**
 * 去除String类型两端空格工具类
 * @author new wei
 * @date 2022/5/13 14:40
 */
public class StringTrimUtil {

    /**
     * 去除实体类中String类型成员变量两端空格
     * @param bean
     * @param <T>
     * @return
     */
    public static <T> T BeanStringAttributeTrimUtil(T bean) throws IllegalAccessException {
        //1.获取字节码对象
        Class<?> clazz = bean.getClass();
        //2.获取全部成员变量
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            //3.获取当前成员变量类型
            Class<?> type = declaredField.getType();
            //4.判断类型是否是String类型
            if (type == String.class){
                //开启暴力反射
                declaredField.setAccessible(true);
                //5.获取当前属性值
                String value = (String) declaredField.get(bean);
                //6.判断是否为null或者空字符串
                if (value == null ||value == ""){
                    continue;
                }
                //7.去除两端空格
                String trimValue = value.trim();
                //8.将去除两端空格后的结果赋值给原变量
                declaredField.set(bean,trimValue);
            }
        }
        //处理完成，返回原对象
        return bean;
    }
}
