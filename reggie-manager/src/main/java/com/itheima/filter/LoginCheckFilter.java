package com.itheima.filter;


import com.itheima.constant.EmployeeConstant;
import com.itheima.util.ThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: jingHaoHao
 * @Desc: Wab容器Servlet过滤器
 * @Date: 2022/6/5 14:57
 */
@Slf4j
@WebFilter
public class LoginCheckFilter implements Filter {
    /**
     *  支持通配符的路径匹配对象
     */
    private static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     *允许放行的请求路径
     */
    private static final String[] passUrl = {
            "/backend/page/login/login.html",
            "/employee/login",
            "/backend/api/**",
            "/backend/images/**",
            "/backend/js/**",
            "/backend/plugins/**",
            "/backend/favicon.ico",
            "/backend/styles/**",
            


    };
    @Override
    /**
     * 功能描述  : 拦截判断
     * @param request
     * @param response
     * @param chain
     * @return : void
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String requestURI = request.getRequestURI();
        log.info("当前请求路径为:" + requestURI);
        //调用方法进行判断该路径是否为需要拦截的路径
        boolean check = check(requestURI);
        //方法返回值为 true,则表示未登录也可以访问的路径
        if (check) {
             chain.doFilter(request,response);
             return;
        }
        //方法返回值为 false,则表示未登录--跳转到登录页面
        //登录才可以访问
        //3）不登录可以访问的资源，直接放行
        String sid = null;
        Cookie[] cookies = request.getCookies();
        //判断cookies是否为空
        if (cookies==null){
            //cookies为空证明用户未登录跳转页面到登录页面
            response.sendRedirect("/backend/page/login/login.html");
            return;
        }
        //cookies为不空证明用户已登录
        for (Cookie cookie : cookies) {
            String name = cookie.getName();
            //判断员工是否登录
            if (EmployeeConstant.EMPLOYEE_TOKIN.equals(name)){
                    sid=cookie.getValue();
            }
        }
        //如果用户没有登录，返回友好错误提示信息
        if (sid==null){
            //用户未登录，返回登录界面进行登录
            response.sendRedirect("/backend/page/login/login.html");
            return;
        }

        String employeeInfo =  stringRedisTemplate.opsForValue().get(sid);
        if (employeeInfo==null){
            //用户未登录，返回登录界面进行登录
            response.sendRedirect("/backend/page/login/login.html");
            return;
        }
        //如果用户已经登录 将用户数据传入当前线程
        if (employeeInfo!=null){
            Long eid  = Long.valueOf(employeeInfo);
            ThreadLocalUtil.setCurrentId(eid);
        }
        log.info("用户已登录，{}，请求放行"+request);
        //用户已经登录，进行放行
        chain.doFilter(request,response);
        return;
    }

    /**
     * 功能描述  : 路径匹配，检查本次请求是否需要放行
     * @param requestURI
     * @return : boolean true-为放行的路径   false-为需要拦截的路径
     */
    public boolean check(String requestURI) {
        boolean flag = false;
        for (String url : passUrl) {
            //判断请求路径是否包含，放行路径
            if (PATH_MATCHER.match(url, requestURI)) {
                flag = true;
                return flag;
            }
        }
        return flag;
    }
}
