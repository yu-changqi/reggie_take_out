package com.itheima.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: zhangHeng
 * @Desc:自定义系统异常
 * @Date: 2022/6/5 19:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemException extends RuntimeException{
    /**
     *状态码
     */
    private String code;

    public SystemException(String code, String message) {
        super(message);
        this.code = code;
    }

    public SystemException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}
