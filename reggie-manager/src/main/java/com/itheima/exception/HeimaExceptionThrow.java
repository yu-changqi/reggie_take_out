package com.itheima.exception;

/**
 * @Author: zhangHeng
 * @Desc:业务异常、系统异常-异常抛出工具类（封装异常抛出代码）
 * @Date: 2022/6/5 19:52
 */
public class HeimaExceptionThrow {

    public static void throwBusinessException(String code,String errMsg){
        //抛出一个带有状态码,信息的业务异常(根据参数传入,更灵活)用的最多
        throw new BusinessException(code, errMsg);
    }

    public static void throwSystemException(String code,String errMsg){
        throw new SystemException(code, errMsg);
    }
}
