package com.itheima.exception;

import com.itheima.common.Result;
import lombok.extern.slf4j.Slf4j;
import com.itheima.constant.SystemCodeConstant;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author: zhangheng
 * @Desc: 增强Controller的通知
 * @Date: 2022-05-02 15:22:48
 */
@RestControllerAdvice
@Slf4j
public class ProjectExceptionAdvice {
    /**
     * @Author: zhangHeng
     * @Desc:异常处理器
     * @Date: 2022/6/5 20:05
     */
    @ExceptionHandler(Exception.class)
    public Result sysEx(Exception e){
        if(e instanceof BusinessException){
            return new Result(((BusinessException)e).getCode(),e.getMessage());
        }
        return new Result(SystemCodeConstant.SYSTEM_UNKNOW_ERROR,e.getMessage());
    }

}
