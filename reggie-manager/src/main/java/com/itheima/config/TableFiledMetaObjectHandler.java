package com.itheima.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.itheima.util.ThreadLocalUtil;
import lombok.SneakyThrows;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Field;
import java.time.LocalDateTime;

/**
 * @Author: Eleike
 * @Desc:  @TableField注解填充策略处理器
 * @Date: 2022/6/5 10:44
 */
@Configuration
public class TableFiledMetaObjectHandler implements MetaObjectHandler {
    /**
     * 功能描述: 创建时自动填充策略
     * @param : metaObject
     * @return : void
     * @SneakyThrows：该注解用在会产生受检异常的代码上，自动将抛出异常的代码包裹上try/catch
     */
    @SneakyThrows
    @Override
    public void insertFill(MetaObject metaObject) {
        //获取用户id(线程中存储的)
        Long empId = ThreadLocalUtil.getCurrentId();
        //获取要自动填充的对象
        Object originalObject = metaObject.getOriginalObject();
        //获取字节码对象
        Class<?> clazz = originalObject.getClass();
        //获取全部成员变量
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            //获取当前成员变量名称
            String name = declaredField.getName();
            //判断如果有需要自动填充的属性才自动填充
            if ("createTime".equals(name)){
                metaObject.setValue("createTime", LocalDateTime.now());
            }
            if ("updateTime".equals(name)){
                metaObject.setValue("updateTime",LocalDateTime.now());
            }
            if ("createUser".equals(name)){
                metaObject.setValue("createUser",empId);
            }
            if ("updateUser".equals(name)){
                metaObject.setValue("updateUser",empId);
            }
        }
    }

    /**
     * 功能描述: 更新时自动填充内容
     * @param : metaObject
     * @return : void
     */
    @SneakyThrows
    @Override
    public void updateFill(MetaObject metaObject) {
        //获取用户id(线程中存储的)
        Long empId = ThreadLocalUtil.getCurrentId();
        //获取要自动填充的对象
        Object originalObject = metaObject.getOriginalObject();
        //获取字节码对象
        Class<?> clazz = originalObject.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            //获取当前成员变量名称
            String name = declaredField.getName();
            //判断如果有需要自动填充的属性才自动填充
            if ("updateTime".equals(name)){
                metaObject.setValue("updateTime",LocalDateTime.now());
            }
            if ("updateUser".equals(name)){
                metaObject.setValue("updateUser",empId);
            }
        }
    }
}
