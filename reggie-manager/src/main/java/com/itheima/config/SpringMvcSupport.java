package com.itheima.config;

import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Eleike
 * @Desc: SpringMVC配置类
 * @Date: 2022/6/5 10:41
 */
@Slf4j
@Configuration
public class SpringMvcSupport extends WebMvcConfigurationSupport {

    /**
     * 功能描述: 设置静态资源映射
     * @param registry
     * @return : void
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("开始进行静态资源映射...");
        registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
    }


    /**
     * 功能描述: 全局-Long、long、BigInteger精度丢失、处理中文乱码问题、日期转换+@JSONField
     * @param converters
     * @return : void
     */
    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 1、定义一个 convert 转换消息的对象;
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        // 2、添加fastJson的配置信息
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(
                ////结果是否格式化,默认为false，配置后则格式化
                SerializerFeature.PrettyFormat,
                //是否输出值为null的字段,默认为false,配置后则输出
                SerializerFeature.WriteMapNullValue,
                //List字段如果为null,输出为[],而非null
                SerializerFeature.WriteNullListAsEmpty,
                //List字段如果为null,输出为[],而非null
                SerializerFeature.WriteNullStringAsEmpty
        );
        // 3.处理中文乱码问题
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON);
        fastConverter.setSupportedMediaTypes(fastMediaTypes);
        // 4、long类型精度丢失
        SerializeConfig serializeConfig = SerializeConfig.globalInstance;
        serializeConfig.put(Long.class , ToStringSerializer.instance);
        serializeConfig.put(Long.TYPE , ToStringSerializer.instance);
        serializeConfig.put(BigInteger.class,ToStringSerializer.instance);
        fastJsonConfig.setSerializeConfig(serializeConfig);
        // 5、fastjson convert中添加配置信息.
        fastConverter.setFastJsonConfig(fastJsonConfig);
        // 6.Fastjson生效
        converters.add(fastConverter);
        converters.add(new StringHttpMessageConverter(StandardCharsets.UTF_8));
    }

}
