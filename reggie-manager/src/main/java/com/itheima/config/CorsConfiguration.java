package com.itheima.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author: Eleike
 * @Desc: 跨域请求配置类
 * @Date: 2022/6/10 8:21
 */
@Configuration
public class CorsConfiguration implements WebMvcConfigurer {
    /**
     * 方法描述 跨域请求配置类
     * @since: 1.0.0
     * @param: registry
     * @return: void
     * @author: Eleike
     * @date: 2022/6/10
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                //请求路径
                .allowedOriginPatterns("*")
                //请求方法
                .allowedMethods("GET")
                //请求头参数
                .allowedHeaders("*")
                //允许携带cookie
                .allowCredentials(true)
                //一次请求后有效时间
                .maxAge(3600);
    }
}
