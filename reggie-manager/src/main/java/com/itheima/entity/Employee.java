package com.itheima.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: liuxinwei
 * @Desc: 用户类-登录
 * @Date: 2022/6/5 14:50
 */
@TableName("employee")
@Data
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 员工ID
     */
    private Long id;
    /**用户名*/
    private String username;
    /**姓名*/
    private String name;
    /**
     * 密码
     */
    private String password;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 性别
     */
    private String sex;
    //驼峰命名法 ---> 映射的字段名为 id_number

    /**
     *  身份证号
     */
    private String idNumber;

    /**
     *  状态
     */
    private Integer status;
    //如果前端传递的是JSON字符串，@JSONformat("")
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建用户
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;
    /**
     * 更新用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
    /**
     * 用户头像
     */
    private String image;
}
