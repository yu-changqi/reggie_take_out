package com.itheima.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: Eleike
 * @Desc: 首页轮播图实体类
 * @Date: 2022/6/7 16:38
 */
@Data
@TableName("home_image")
@AllArgsConstructor
@NoArgsConstructor
public class HomeImage implements Serializable {
    /**
     * 主键
     */
    private Long id;
    /**
     * 图片名称
     */
    private String name;
    /**
     * 存储地址
     */
    private String image;
    /**
     * 跳转链接
     */
    private String link;
    /**
     * 介绍
     */
    private String description;
    /**
     * 创建时间 (JSON日期格式 + 自动填充)
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;
    /**
     * 修改人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
}
