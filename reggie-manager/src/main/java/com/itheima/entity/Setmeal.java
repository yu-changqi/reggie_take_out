package com.itheima.entity;


import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author: jingHaoHao
 * @Desc: 套餐实体类
 * @Date: 2022/6/5 15:51
 */
@Data
@TableName("setmeal")
public class Setmeal implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *套餐管理主键-id
     */
    private Long id;
    /**
     *菜品分类id
     */
    private Long categoryId;
    /**
     *套餐名称
     */
    private String name;
    /**
     *套餐价格
     */
    private BigDecimal price;
    /**
     *套餐管理状态 0:停用 1:启用
     */
    private Integer status;
    /**
     *编码
     */
    private String code;
    /**
     *描述信息
     */
    private String description;
    /**
     *图片
     */
    private String image;
    /**
     *创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     *更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime updateTime;
    /**
     *创建人
     */
    @TableField(fill = FieldFill.INSERT)

    private Long createUser;
    /**
     *修改人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
    /**
     *逻辑删除- 0-未删除 1-删除
     */
    @TableLogic
    private Integer isDeleted;


}
