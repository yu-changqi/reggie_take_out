package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Setmeal;
/**
 * @Author: jingHaoHao
 * @Desc: 套餐管理--Mapper层
 * @Date: 2022/6/5 17:44
 */
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
