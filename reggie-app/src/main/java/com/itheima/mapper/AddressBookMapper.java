package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.AddressBook;
/**
 * @Author: qiChangYue
 * @Desc:用户地址模块-DAO接口
 * @Date: 2022/6/8 17:26
 */

public interface AddressBookMapper extends BaseMapper<AddressBook> {
}
