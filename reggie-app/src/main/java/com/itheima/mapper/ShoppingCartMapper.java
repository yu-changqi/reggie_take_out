package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.ShoppingCart;

/**
 * @Author: GengZhiFei
 * @Desc: 【瑞吉-app端】-【购物车模块】-【购物车mapper接口】
 * @Date: 2022/6/8 14:47
 */
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {
}
