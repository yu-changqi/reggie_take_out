package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Dish;
/**
 * @Author: jingHaoHao
 * @Desc:   菜品-Mapper
 * @Date: 2022/6/8 16:28
 */

public interface DishMapper extends BaseMapper<Dish> {
}
