package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Category;
/**
 * @Author: jingHaoHao
 * @Desc:   分类-Mapper层
 * @Date: 2022/6/8 15:41
 */

public interface CategoryMapper extends BaseMapper<Category> {
}
