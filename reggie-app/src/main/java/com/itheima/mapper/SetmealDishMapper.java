package com.itheima.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.SetmealDish;

public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
