package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.vo.OrderVO;
import com.itheima.entity.Order;
import com.itheima.vo.OrderVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper extends BaseMapper<Order> {

		List<OrderVO> findPage(@Param("userId") Long userId, @Param("start") Integer start, @Param("size") Integer size);
}
