package com.itheima.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author: qiChangYue
 * @Desc:订单实体类
 * @Date: 2022/6/8 14:30
 */
@Data
@TableName("orders")
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {
    /**
     * 主键
     */
    private Long id;
    /**
     * 订单号
     */
    private String number;
    /**
     * 订单状态 1待付款，2待派送，3已派送，4已完成，5已取消
     */
    private Integer status;
    /**
     * 下单用户
     */
    private Long userId;
    /**
     * 收货地址id
     */
    private Long addressBookId;
    /**
     * 下单时间
     */
    private LocalDateTime orderTime;
    /**
     * 结账时间
     */
    private LocalDateTime checkoutTime;
    /**
     * 支付方式 1微信,2支付宝
     */
    private Integer payMethod;
    /**
     * 实收金额
     */
    private BigDecimal amount;
    /**
     * 备注
     */
    private String remark;
    /**
     * 收货人手机号
     */
    private String phone;
    /**
     * 收货人地址
     */
    private String address;
    /**
     * 下单用户姓名
     */
    private String userName;
    /**
     * 收货人姓名
     */
    private String consignee;
}
