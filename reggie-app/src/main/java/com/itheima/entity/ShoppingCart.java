package com.itheima.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Shopping_cart
 *  购物车-实体类
 * @author lishuo
 * @date 2022/6/8
 */
@Data
public class ShoppingCart implements Serializable {
    private Long id;
    /**
     *  名称
     */
    private String name;
    /**
     *  图片
     */
    private String image;
    /**
     *  用户id
     */
    private Long userId;
    /**
     *  菜品id
     */
    private Long dishId;
    /**
     *  套餐id
     */
    private Long setmealId;
    /**
     *  口味
     */
    private String dishFlavor;
    /**
     *  数量
     */
    private Integer number;
    /**
     * 金额
     */
    private BigDecimal amount;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
