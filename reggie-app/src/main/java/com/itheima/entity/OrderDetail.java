package com.itheima.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: qiChangYue
 * @Desc:订单详情实体类
 * @Date: 2022/6/8 14:26
 */
@Data
@TableName("order_detail")
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail implements Serializable {
    /**
     * 主键
     */
    private Long id;
    /**
     * 菜品/套餐名称
     */
    private String name;
    /**
     * 图片
     */
    private String image;
    /**
     * 订单id
     */
    private Long orderId;
    /**
     * 菜品id
     */
    private Long dishId;
    /**
     * 套餐id
     */
    private Long setmealId;
    /**
     * 菜品口味
     */
    private String dishFlavor;
    /**
     * 数量
     */
    private Integer number;
    /**
     * 单价
     */
    private BigDecimal amount;
}
