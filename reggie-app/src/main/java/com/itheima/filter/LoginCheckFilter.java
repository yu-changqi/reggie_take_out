package com.itheima.filter;

import com.itheima.constant.UserConstant;
import com.itheima.utils.ThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: zhangHeng
 * @Desc:Web容器Servlet过滤器
 * @Date: 2022/6/8 10:12
 */
@WebFilter
@Slf4j
@Configuration
public class LoginCheckFilter implements Filter {
    /**
     * 支持通配符的路径匹配对象
     */
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     *请求放行的路径
     */
    private static final String[] passUrls={
            "/front/page/login.html",
            "/front/index.html",
            "/front/api/**",
            "/front/images/**",
            "/front/styles/**",
            "/front/js/**",
            "/front/fonts/**",
            "/front/plugins/**",
            "/user/sendMsg",
            "/user/login*",
            "/category/list*",
            "/dish/list*",
            "/setmeal/list*",
            "/setmeal/dish/*"
    };
    /**
     * 功能描述: 拦截判断
     * @return : void
     * @param servletRequest
     * @param servletResponse
     * @param chain
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String requestURI = request.getRequestURI();
        //log.info("请求路径为：{}",requestURI);
        //判断请求路径属于哪一类
        boolean check = check(requestURI);
        if (PATH_MATCHER.match("/shoppingCart/list*",requestURI)) {
            System.out.println(requestURI);
        }

        //返回值为true则放行
        if(check){
            chain.doFilter(request,response);//放行操作
            return;
        }
        //用户登录才可以访问
        String userId = null;
        Cookie[] cookies = request.getCookies();
        if (cookies==null){
            //cookies为空证明用户未登录跳转页面到登录页面
            response.sendRedirect("/front/page/login.html");
            return;
        }
        for (Cookie cookie : cookies) {
            String token = (String)cookie.getName();
            //判断是否有用户登录
            if(UserConstant.USER_LONGIN_TOKEN.equals(token)){
                userId = cookie.getValue();
            }
        }
        //如果用户没有登录，返回友好错误提示信息
        if (userId==null){
            //用户未登录，返回登录界面进行登录
            response.sendRedirect("/front/page/login.html");
            return;
        }
        String userInfo =  stringRedisTemplate.opsForValue().get(userId);
        if (userInfo==null){
            //用户未登录，返回登录界面进行登录
            response.sendRedirect("/front/page/login.html");
            return;
        }
        //如果用户已经登录 将用户数据传入当前线程
        Long eid  = Long.valueOf(userInfo);
        ThreadLocalUtil.setThread(eid);
        //用户已经登录，进行放行
        chain.doFilter(request,response);
        return;
    }
    /**
     * 路径匹配，检查本次请求是否需要放行
     * @param requestURI
     * @return true-放行,false-拦截
     */
    public boolean check(String requestURI){
        boolean flag = false;
        for (String  url: passUrls) {
            //判断请求路径是否包含，放行路径
            if(PATH_MATCHER.match(url,requestURI)){
                flag = true;
                return flag;
            }
        }
        return flag;
    }
}
