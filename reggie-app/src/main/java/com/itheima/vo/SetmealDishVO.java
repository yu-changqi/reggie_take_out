package com.itheima.vo;


import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author: jingHaoHao
 * @Desc: 套餐--套餐视图对象
 * @Date: 2022/6/5 15:51
 */
@Data
public class SetmealDishVO implements Serializable {
    /**
     *  套餐菜品关系表-主键
     */
    private Long id;
    /**
     *  套餐菜品关系表-套餐id
     */
    private Long setmealId;
    /**
     *  套餐菜品关系表-菜品id
     */
    private Long dishId;
    /**
     *  套餐菜品关系表-菜品名称
     */
    private String name;
    /**
     *  套餐菜品关系表-菜品价格
     */
    private BigDecimal price;
    /**
     *  套餐菜品关系表-份数
     */
    private Integer copies;
    /**
     *  套餐菜品关系表-排序
     */
    private Integer sort;
    /**
     *  套餐菜品关系表-创建时间
     */

    @TableField(fill = FieldFill.INSERT)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     *  套餐菜品关系表-修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     *  套餐菜品关系表-创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;
    /**
     *  套餐菜品关系表-修改人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
    /**
     *  套餐菜品关系表-逻辑删除  0-未删除 1-删除
     */
    private Integer isDeleted;
    /**
     *菜品描述
     */
    private String description;


}
