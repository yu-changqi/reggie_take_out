package com.itheima.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Eleike
 * @Desc: 标准结果集返回封装
 * @Date: 2022/6/5 15:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result implements Serializable {
    /**
     * 状态码
     */
    private String code;
    /**
     * 状态信息
     */
    private String msg;
    /**
     * 数据
     */
    private Object data;

    public Result(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
