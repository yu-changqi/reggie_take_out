package com.itheima.utils;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @Author: zhuan
 * @Desc: 阿里云短信发送工具类
 * @Date: 2022/6/8 12:54
 */
@Slf4j
public class AliyunSmsUtil {
    /**
     * 短信发送成功状态码
     */
    private static final String OK = "OK";
    //-------------------------阿里云短信发送相关参数设置，保持不变即可------------------
    /**
     * 阿里云服务器地域选择  cn-hangzhou-杭州
     */
    private static final String REGION_ID = "cn-hangzhou";
    /**
     * 阿里短信服务域名
     */
    private static final String DOMAIN = "dysmsapi.aliyuncs.com";
    /**
     * 版本信息
     */
    private static final String VERSION = "2017-05-25";
    /**
     * 行为，发送短信
     */
    private static final String ACTION = "SendSms";
    /**
     * 短信服务所需AK
     */
    private static final String ACCESS_KEY_ID = "LTAI5tFc8QJM7W3KZkeQ41vr";
    /**
     * 短信服务所需SK 用户权限相关
     */
    private static final String SECRET = "RRTwtuzn40ihcqleY9EpgXiuElrvoc";
    //-------------------------阿里云短信发送模板相关参数，可自行更改------------------
    /**
     * 签名名称
     */
    private static final String SIGN_NAME = "Eleike";
    /**
     * 短信模板code
     */
    private static final String TEMPLATE_CODE = "SMS_242550757";

    /**
     * 功能描述: 阿里云短信发送接口，支持通知、验证码、激活码类短信
     * @param phoneNumber, templateParam
     *  map中所需参数，参数名称固定，如下(注意：参数首字母都是小写的)：
     *      phoneNumbers ： 手机号码，支持多个中间以“,”英文逗号分隔 （必填）
     *      signName ：签名（必填）
     *      templateCode ：模板code（必填）
     *      templateParam ：模板参数（不必填，如果是通知类短信，该参数可以不设置）
     * @return : java.lang.Boolean
     *  true-短信发送成功；false-短信发送失败
     */
    public static Boolean sendSms(String phoneNumber,Object templateParam){
        Boolean flag = false;
        DefaultProfile profile = DefaultProfile.getProfile(REGION_ID, ACCESS_KEY_ID, SECRET);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        //发送短信域名，不要改变
        request.setSysDomain(DOMAIN);
        request.setSysVersion(VERSION);
        request.setSysAction(ACTION);
        //短信发送的具体信息参数
        request.putQueryParameter("PhoneNumbers", phoneNumber);
        request.putQueryParameter("SignName", SIGN_NAME);
        request.putQueryParameter("TemplateCode", TEMPLATE_CODE);
        //通知类短信，模板不需要参数。判断传递的模板参数是否为空，如果为空可能是通知类短信，则不需要添加模板参数条件
        if(templateParam != null){
            if (templateParam instanceof String){
                templateParam = ((String) templateParam).trim();
            }
            templateParam = "{\"code\":\"" + templateParam + "\"}";
            request.putQueryParameter("TemplateParam", (String) templateParam);
        }
        try {
            //发送短信
            CommonResponse response = client.getCommonResponse(request);
            //发送短信结果转为Map类型 jackson方式
            Map<String,String> responseMap = new ObjectMapper().readValue(response.getData(),Map.class);
            //判断短信发送是否成功
            if(OK.equals(responseMap.get("Code"))){

                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
}