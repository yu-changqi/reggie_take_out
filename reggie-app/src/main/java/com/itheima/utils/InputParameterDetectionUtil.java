package com.itheima.utils;

/**
 * @Author: Eleike
 * @Desc: 输入参数测试工具类
 * @Date: 2022/6/9 16:44
 */
public class InputParameterDetectionUtil {
    /**
     * 手机号检测正则表达式
     */
    private static final String PHONE_STANDARD = "(13[0-9]{9})|(15[0-9]{9})|(17[0-9]{9})|(18[0-9]{9})|(19[0-9]{9})";
    /**
     * 身份证号检测正则表达式
     */
    private static final String ID_CARD_STANDARD = "(\\d{15})|(\\d{18})|(\\d{17}(\\d|X|x))";

    /**
     * 方法描述 手机号检测
     * @since: 1.0.0
     * @param: phone
     * @return: boolean
     * @author: Eleike
     * @date: 2022/6/9
     */
    public static boolean phoneNumberDetection(String phone){
        boolean flag = false;
        //判断手机号是否合格
        if (phone.matches(PHONE_STANDARD)){
            flag = true;
            return flag;
        }
        return flag;
    }

    /**
     * 方法描述 身份证号检测
     * @since: 1.0.0
     * @param: phone
     * @return: boolean
     * @author: Eleike
     * @date: 2022/6/9
     */
    public static boolean idCardDetection(String idCard){
        boolean flag = false;
        //判断手机号是否合格
        if (idCard.matches(ID_CARD_STANDARD)){
            flag = true;
            return flag;
        }
        return flag;
    }
}
