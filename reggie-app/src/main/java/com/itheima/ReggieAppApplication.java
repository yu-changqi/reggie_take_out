package com.itheima;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @Author: zhuan
 * @Desc: 瑞吉点餐-客户端APP
 * @Date: 2022-06-04 22:01:30
 */
@Slf4j
@EnableWebMvc
@MapperScan("com.itheima.mapper")
@ServletComponentScan("com.itheima.filter")
@SpringBootApplication
public class ReggieAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReggieAppApplication.class, args);
        log.info("项目启动成功!");
    }

}
