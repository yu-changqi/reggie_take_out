package com.itheima.controller;

import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.common.PageResult;
import com.itheima.common.Result;
import com.itheima.vo.OrderVO;
import com.itheima.entity.Order;
import com.itheima.service.OrderService;
import com.sun.org.apache.bcel.internal.classfile.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: zhangheng
 * @Desc: 订单模块-控制器
 * @Date: 2022-06-09 11:16:12
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 功能描述: 提交订单
     *
     * @param orders
     * @return : com.itheima.common.Result
     */
    @PostMapping("/submit")
    public Result submitOrder(@RequestBody Order orders) {
        int row = 0;
        //1.调用Service,返回业务处理结果
        row = orderService.submitOrder(orders);
        //2.封装返回
        if (row < 1) {
            return new Result(BusinessCodeConstant.ORDER_SUBMIT_ERROR, BusinessMessageConstant.ORDER_SUB_ERROR);
        }
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.ORDER_SUBMIT_OK);
    }

    /**
     * 功能描述: 查询订单列表
     *
     * @param page
     * @param pageSize
     * @return : com.itheima.common.PageResult
     */
    @GetMapping("/userPage")
    public PageResult page(@RequestParam("page") Integer page, @RequestParam("pageSize") Integer pageSize) {
        List<OrderVO> vos = orderService.page(page, pageSize);
        if (vos == null) {
            vos = new ArrayList<>();
        }
        return new PageResult(BusinessCodeConstant.OK, BusinessMessageConstant.ORDER_SELECT_OK, vos);
    }
    /*
     * 功能描述:再来一单菜品
     * @return : Result
     * @param map
     */
    @PostMapping("/again")
	public Result againList(@RequestBody Order order){
        Long id = order.getId();
        Order o=orderService.againList(id);
        if (o==null) {
            return new Result(BusinessCodeConstant.ORDER_AGAIN_ERROR,BusinessMessageConstant.ORDER_AGAIN_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.ORDER_SELECT_OK);
    }
}
