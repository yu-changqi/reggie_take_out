package com.itheima.controller;

import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.constant.CommonConstant;
import com.itheima.entity.AddressBook;
import com.itheima.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Eleike
 * @Desc: 用户地址处理器
 * @Date: 2022/6/8 21:03
 */
@RestController
@RequestMapping("/addressBook")
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;

    /**
     * 方法描述 获取用户地址列表
     * @since: 1.0.0
     * @param:
     * @return: Resul
     * @author: Eleike
     * @date: 2022/6/9
     */
    @GetMapping("/list")
    public Result selectAddressBookListByUserId(){
        List<AddressBook> addressBookList = addressBookService.selectAddressBookListByUserId();
        if (addressBookList == null){
            addressBookList = new ArrayList<>();
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.USER_SELECT_ADDRESS_BOOK_LIST_SUCCESS,addressBookList);
    }

    /**
     * 方法描述 用户地址添加请求
     * @since: 1.0.0
     * @param: addressBook
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/9
     */
    @PostMapping
    public Result addAddressBookByUserId(@RequestBody AddressBook addressBook) throws IllegalAccessException {
        int rows = addressBookService.addAddressBookByUserId(addressBook);
        if (rows < 1){
            return new Result(BusinessCodeConstant.USER_ADD_ADDRESS_BOOK_ERROR,BusinessMessageConstant.USER_ADD_ADDRESS_BOOK_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.USER_ADD_ADDRESS_BOOK_SUCCESS,rows);
    }

    /**
     * 方法描述 修改用户默认地址
     * @since: 1.0.0
     * @param: addressBook
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/9
     */
    @PutMapping("/default")
    public Result updateDefaultAddressBookByUserId(@RequestBody AddressBook addressBook){
        int rows = addressBookService.updateDefaultAddressBookByUserId(addressBook);
        if (rows < 1){
            return new Result(BusinessCodeConstant.USER_UPDATE_DEFAULT_ADDRESS_BOOK_ERROR,BusinessMessageConstant.USER_UPDATE_DEFAULT_ADDRESS_BOOK_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.USER_UPDATE_DEFAULT_ADDRESS_BOOK_SUCCESS,rows);
    }

    /**
     * 方法描述 查询用户指定地址信息
     * @since: 1.0.0
     * @param: id
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/9
     */
    @GetMapping("/{id}")
    public Result selectUserAddressBookByAddressId(@PathVariable("id") Long id){
        AddressBook addressBook = addressBookService.selectUserAddressBookByAddressId(id);
        if (addressBook == null){
            return new Result(BusinessCodeConstant.USER_SELECT_ADDRESS_BOOK_ERROR,BusinessMessageConstant.USER_SELECT_ADDRESS_BOOK_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.USER_SELECT_ADDRESS_BOOK_SUCCESS,addressBook);
    }

    /**
     * 方法描述 删除指定地址
     * @since: 1.0.0
     * @param: ids
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/9
     */
    @DeleteMapping
    public Result deleteUserAddressBookByAddressIds(Long[] ids){
        int rows = addressBookService.deleteUserAddressBookByAddressIds(ids);
        if (rows < 1){
            return new Result(BusinessCodeConstant.USER_DELETE_ADDRESS_BOOK_ERROR,BusinessMessageConstant.USER_DELETE_ADDRESS_BOOK_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.USER_DELETE_ADDRESS_BOOK_SUCCESS,rows);
    }

    /**
     * 方法描述 修改用户地址信息
     * @since: 1.0.0
     * @param: addressBook
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/9
     */
    @PutMapping
    public Result updateUserAddressBookByAddress(@RequestBody AddressBook addressBook) throws IllegalAccessException {
        int rows = addressBookService.updateUserAddressBookByAddress(addressBook);
        if (rows < 1){
            return new Result(BusinessCodeConstant.USER_UPDATE_ADDRESS_BOOK_ERROR,BusinessMessageConstant.USER_UPDATE_ADDRESS_BOOK_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.USER_UPDATE_ADDRESS_BOOK_SUCCESS,rows);
    }
    /**
     * 方法描述 获取默认地址
     * @since: 1.0.0
     * @param:
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/11
     */
    @GetMapping("/default")
    public Result getDefaultAddressBookByUserId(){
        AddressBook addressBook = addressBookService.getDefaultAddressBookByUserId();
        if (addressBook == null){
            return new Result(BusinessCodeConstant.USER_SELECT_ADDRESS_BOOK_ERROR,BusinessMessageConstant.USER_SELECT_ADDRESS_BOOK_ERROR);
        }
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.USER_SELECT_ADDRESS_BOOK_SUCCESS,addressBook);
    }

    /**
     * 方法描述 用户地址数量检测
     * @since: 1.0.0
     * @param:
     * @return: Result
     * @author: Eleike
     * @date: 2022/6/11
     */
    @GetMapping("/count")
    public Result getUserAddressCount(){
        Integer count = addressBookService.getUserAddressCount();
        if (count >= CommonConstant.USER_MAX_ADDRESS_COUNT){
            return new Result(BusinessCodeConstant.USER_ADDRESS_BOOK_COUNT_MAX,BusinessMessageConstant.USER_ADDRESS_BOOK_COUNT_MAX);
        }
        Result result = new Result();
        result.setCode(BusinessCodeConstant.OK);
        return result;
    }
}
