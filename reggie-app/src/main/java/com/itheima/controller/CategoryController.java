package com.itheima.controller;


import com.itheima.common.Result;
import com.itheima.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Author: jingHaoHao
 * @Desc:分类管理-Controller
 * @Date: 2022/6/8 15:58
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 功能描述  : 查询分类列表
     * @return : com.itheima.common.Result
     */
    @GetMapping("/list")
    public Result find(){
        return categoryService.findAll();
    }
}
