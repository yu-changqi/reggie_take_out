package com.itheima.controller;

import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.entity.ShoppingCart;
import com.itheima.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: GengZhiFei
 * @Desc: 【瑞吉-app端】-【购物车模块】-【控制器】
 * @Date: 2022/6/8 14:54
 */
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 功能描述: 将菜品添加到购物车
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    private Result addShoppingCart(@RequestBody ShoppingCart shoppingCart) {
        //调用service业务层
        ShoppingCart data = shoppingCartService.addShoppingCart(shoppingCart);
        if (data == null) {
            return new Result(BusinessCodeConstant.SHOPPINGCART_ADD_DISH_ERROR, BusinessMessageConstant.SHOPPINGCART_ADD_DISH_ERROR);
        }
        //封装结果数据, 返回成功状态码,成功提示,查询数据
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SHOPPINGCART_ADD_DISH_SUCCESS, data);
    }
    /**
     * 功能描述: 获取购物车列表
     * @return
     */
    @GetMapping("/list")
    public Result getShoppingCartListData() {
        //调用service业务层
        List<ShoppingCart> list = shoppingCartService.getShoppingCartListData();
        if (list == null) {
            return new Result(BusinessCodeConstant.SHOPPINGCART_GET_LIST_ERROR, BusinessMessageConstant.SHOPPINGCART_GET_LIST_ERROR);
        }
        //返回成功状态码,成功提示,集合数据
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SHOPPINGCART_GET_LIST_SUCCESS, list);
    }
    /**
     * 功能描述: 清空购物车
     * @return
     */
    @DeleteMapping("/clean")
    public Result emptyTheShoppingCartAllData() {
        //调用service业务层
        int row = shoppingCartService.emptyTheShoppingCartAllData();
        if (row < 1) {
            return new Result(BusinessCodeConstant.SHOPPINGCART_CLEAN_ERROR,BusinessMessageConstant.SHOPPINGCART_CLEAN_ERROR);
        }
        //封装结果数据, 返回成功状态码,成功提示
        return new Result(BusinessCodeConstant.OK,BusinessMessageConstant.SHOPPINGCART_CLEAN_SUCCESS);
    }
    /**
     * 功能描述: 减少菜品数量, 大于 1份减 1, 等于 1份(物理)删除
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    private Result reduceDishCount(@RequestBody ShoppingCart shoppingCart) {
        //调用service业务层
        ShoppingCart data= shoppingCartService.reduceDishCount(shoppingCart);
        if (data == null) {
            return new Result(BusinessCodeConstant.SHOPPINGCART_REDUCE_COUNT_ERROR, BusinessMessageConstant.SHOPPINGCART_REDUCE_COUNT_ERROR);
        }
        //封装结果数据, 返回成功状态码,成功提示,数据
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SHOPPINGCART_REDUCE_COUNT_SUCCESS, data);
    }

}
