package com.itheima.controller;

import com.itheima.common.Result;
import com.itheima.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: jingHaoHao
 * @Desc:套餐管理系统--Controller层
 * @Date: 2022/6/6 11:02
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;


    /**
     * 功能描述  :前端查询-套餐
     * @param categoryId
     * @param status
     * @return : com.itheima.common.Result
     */
    @GetMapping("/list")
    public Result findStatus(@RequestParam("categoryId") Long categoryId,@RequestParam("status") Integer status){
        return setmealService.findStatus(categoryId,status);
    }

    /**
     * 功能描述  : 菜品和套餐详细信息查询
     * @param id
     * @return : com.itheima.common.Result
     */
    @GetMapping("/dish/{id}")
    public Result findById(@PathVariable Long id) {
        return setmealService.findById(id);
    }
}
