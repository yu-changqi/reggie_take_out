package com.itheima.controller;

import com.itheima.common.Result;
import com.itheima.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Author: jingHaoHao
 * @Desc:菜品-Controller
 * @Date: 2022/6/8 16:29
 */
@RestController
@RequestMapping("/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    @GetMapping("/list")
    public Result findDish(@RequestParam("categoryId") Long categoryId,@RequestParam("status") Integer status){
        return dishService.findDish(categoryId,status);
    }

}
