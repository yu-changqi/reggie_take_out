package com.itheima.constant;

/**
 * @Author: Eleike
 * @Desc: 业务状态提示信息
 * @Date: 2022/6/8 10:29
 */
public class BusinessMessageConstant {
    ////////////////////////////////////公用提示信息//////////////////////////////////////
    /*================================分页查询==================================*/
    /**
     * 分页查询成功提示信息
     */
    public static final String PAGE_SELECT_SUCCESS = "分页查询成功";
    /**
     * 分页查询失败提示信息
     */
    public static final String PAGE_SELECT_ERROR = "分页查询失败，请重试";
    /*================================文件上传==================================*/
    /**
     * 文件上传成功提示
     */
    public static final String UPLOAD_FILE_SUCCESS = "文件上传成功！";
    /**
     * 上传文件为空提示
     */
    public static final String UPLOAD_FILE_NOT_SELECTED = "文件未选择，请先选择文件！";
    /**
     * 上传路径没有写权限提示
     */
    public static final String UPLOAD_NOT_HAVE_WRITER_PERMISSION = "上传目录没有写权限！";
    /**
     * 上传目录名不正确提示
     */
    public static final String UPLOAD_DIR_IS_INCORRECT = "目录名不正确！";
    /**
     * 上传文件爱你大小超过限制提示
     */
    public static final String UPLOAD_FILE_SIZE_EXCEEDS_LIMIT = "上传文件大小超过限制！";
    /**
     * 上传文件扩展名不合理提示
     */
    public static final String UPLOAD_FILE_EXT_IS_NOT_ALLOWED = "上传文件扩展名是不允许的扩展名！";


    ////////////////////////////////////APP首页提示信息//////////////////////////////////////
    /*=============================分类管理查询=====================================*/
    /**
     * 分类查询
     */
    public static final String CATEGORY_GET_SUCCESS="分类查询成功";
    /***
     * 菜品查询
     */
    public static final String DISH_GET_SUCCESS="菜品列表查询成功";
    /**
     *套餐查询
     */
    public static final String SETMEAL_GET_SUCCESS="套餐列表查询成功";
    /**
     * 菜品详细查询成功
     */
    public static final String DISH_DETAILED_GET_SUCCESS="菜品详细查询成功";
    /**
     * 套餐详细信息查询成功
     */
    public static final String SETMEAL_DETAILED_GET_SUCCESS="套餐列表查询成功";

    ////////////////////////////////////登录模块提示信息//////////////////////////////////////
    /**
     * 登录手机号为空
     */
    public static final String LOGIN_PHONE_EMPTY_ERROR ="登录手机号为空，请校验后重试！";
    /**
     * 登录成功消息提示
     */
    public static final String LOGIN_SUCCESS ="登录成功！";
    /**
     * 登录成功消息提示
     */
    public static final String SEND_MSG_SUCCESS ="发送验证码成功！";
    /**
     * 发送验证码失败提示信息
     */
    public static final String SEND_MSG_ERROR ="发送验证码失败，请稍后重试！";
    /**
     * 手机格式不正确提示信息
     */
    public static final String PHONE_FORMAT_ERROR ="手机号格式不正确，请重新输入！";
    /**
     * 验证码输入错误提示信息
     */
    public static final String MSG_CHECK_ERROR ="验证码输入有误，请核对后重试！";
    /**
     * 登出成功消息提示
     */
    public static final String LOGIN_OUT_SUCCESS ="登出成功！";
    /**
     * 登出失败消息提示
     */
    public static final String LOGIN_OUT_ERROR ="退出失败，请稍后重试！";
    //////////////////////////////////////个人中心A1200~A1299///////////////////////////////////////////
    /*===================================地址列表=====================================*/
    /**
     * 用户地址列表获取成功
     */
    public static final String USER_SELECT_ADDRESS_BOOK_LIST_SUCCESS = "用户地址列表获取成功";
    /**
     * 用户地址列表获取失败
     */
    public static final String USER_SELECT_ADDRESS_BOOK_LIST_ERROR = "用户地址列表获取失败，请刷新重试!";
    /**
     * 用户地址添加成功
     */
    public static final String USER_ADD_ADDRESS_BOOK_SUCCESS = "用户地址添加成功";
    /**
     * 用户地址添加失败
     */
    public static final String USER_ADD_ADDRESS_BOOK_ERROR = "用户地址添加失败，请检查后重试!";
    /**
     * 用户地址添加时收货人为空
     */
    public static final String USER_ADD_ADDRESS_CONSIGNEE_IS_NULL = "请填写收货人信息";
    /**
     * 用户地址添加时性别为空
     */
    public static final String USER_ADD_ADDRESS_SEX_IS_NULL = "请选择收货人性别";
    /**
     * 用户地址添加时手机号为空
     */
    public static final String USER_ADD_ADDRESS_PHONE_IS_NULL = "请填写收货人手机号";
    /**
     * 用户地址添加时手机号不正确
     */
    public static final String USER_ADD_ADDRESS_PHONE_IS_FALSE = "收货人手机号不正确，请重新输入正确的手机号";
    /**
     * 用户地址添加时详细地址为空
     */
    public static final String USER_ADD_ADDRESS_DETAIL_IS_NULL = "请填写详细地址";
    /**
     * 用户修改默认地址成功
     */
    public static final String USER_UPDATE_DEFAULT_ADDRESS_BOOK_SUCCESS = "默认地址修改成功";
    /**
     * 用户修改默认地址失败
     */
    public static final String USER_UPDATE_DEFAULT_ADDRESS_BOOK_ERROR = "默认地址修改失败，请确认后重试";
    /**
     * 用户查询指定地址成功
     */
    public static final String USER_SELECT_ADDRESS_BOOK_SUCCESS = "地址信息获取成功";
    /**
     * 用户查询指定地址失败
     */
    public static final String USER_SELECT_ADDRESS_BOOK_ERROR = "地址信息获取失败，请刷新重试";
    /**
     * 用户删除指定地址成功
     */
    public static final String USER_DELETE_ADDRESS_BOOK_SUCCESS = "删除地址成功";
    /**
     * 用户删除指定地址失败
     */
    public static final String USER_DELETE_ADDRESS_BOOK_ERROR = "删除地址失败，请刷新重试";
    /**
     * 用户修改指定地址信息成功
     */
    public static final String USER_UPDATE_ADDRESS_BOOK_SUCCESS = "修改地址信息成功";
    /**
     * 用户修改指定地址信息失败
     */
    public static final String USER_UPDATE_ADDRESS_BOOK_ERROR = "修改地址信息是啊比，请确认后重试";
    /**
     * 用户地址数量达到最大上限
     */
    public static final String USER_ADDRESS_BOOK_COUNT_MAX = "用户地址数量达上限，请充值";
    //////////////////////////////////////购物车///////////////////////////////////////////
    /**
     * 购物车新增菜品成功
     */
    public static final String SHOPPINGCART_ADD_DISH_SUCCESS = "购物车新增菜品成功";
    /**
     * 购物车新增菜品失败
     */
    public static final String SHOPPINGCART_ADD_DISH_ERROR = "购物车新增菜品失败";
    /**
     * 获取购物车列表成功
     */
    public static final String SHOPPINGCART_GET_LIST_SUCCESS = "获取购物车列表成功";
    /**
     * 获取购物车列表失败
     */
    public static final String SHOPPINGCART_GET_LIST_ERROR = "获取购物车列表失败";
    /**
     * 清空购物车成功
     */
    public static final String SHOPPINGCART_CLEAN_SUCCESS = "清空购物车成功";
    /**
     * 清空购物车失败
     */
    public static final String SHOPPINGCART_CLEAN_ERROR = "清空购物车失败";
    /**
     * 减少购物车菜品数量成功
     */
    public static final String SHOPPINGCART_REDUCE_COUNT_SUCCESS = "减少购物车菜品数量成功";
    /**
     * 减少购物车菜品数量失败
     */
    public static final String SHOPPINGCART_REDUCE_COUNT_ERROR = "减少购物车菜品数量失败";
    ////////////////////////////////////订单信息//////////////////////////////////////
    /**
     *  订单提交失败
     */
    public static final String ORDER_SUB_ERROR = "订单提交失败";
    /**
     *  订单提交成功
     *
     */
    public static final String ORDER_SUBMIT_OK = "订单提交成功";
    /**
     *  查询订单成功
     *
     */
    public static final String ORDER_SELECT_OK = "查询订单成功";
    /**
     *  再来一单失败
     */
    public static final String ORDER_AGAIN_ERROR = "再来一单失败";
    /**
     *  再来一单成功
     */
    public static final String ORDER_AGAIN_OK = "再来一单成功";

}
