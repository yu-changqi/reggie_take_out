package com.itheima.constant;

/**
 * @Author: Eleike
 * @Desc: 业务功能状态码
 * @Date: 2022/6/8 10:14
 */
public class BusinessCodeConstant {
    /**
     * 成功执行状态码
     */
    public static final String OK = "00000";
    //////////////////////////////////////公用状态码A1000~A1099///////////////////////////////////////////
    /*=================================分页查询===================================*/
    /**
     * 分页查询失败
     */
    public static final String PAGE_SELECT_ERROR = "A1001";

    /*================================文件上传==================================*/
    /**
     * 文件不存在
     */
    public static final String UPLOAD_FILE_NOT_SELECTED = "A1011";
    /**
     * 上传目录没有写权限
     */
    public static final String UPLOAD_NOT_HAVE_WRITER_PERMISSION = "A1012";
    /**
     * 目录名不正确
     */
    public static final String UPLOAD_DIR_IS_INCORRECT = "A1013";
    /**
     * 文件大小超出限制
     */
    public static final String UPLOAD_FILE_SIZE_EXCEEDS_LIMIT = "A1014";
    /**
     * 上传文件扩展名不允许
     */
    public static final String UPLOAD_FILE_EXT_IS_NOT_ALLOWED = "A1015";
    //////////////////////////////////////购物车A1400~A1499///////////////////////////////////////////
    /**
     * 购物车新增菜品失败
     */
    public static final String SHOPPINGCART_ADD_DISH_ERROR = "A1411";
    /**
     * 获取购物车列表失败
     */
    public static final String SHOPPINGCART_GET_LIST_ERROR = "A1421";
    /**
     * 清空购物车失败
     */
    public static final String SHOPPINGCART_CLEAN_ERROR = "A1431";
    /**
     * 减少购物车菜品数量失败
     */
    public static final String SHOPPINGCART_REDUCE_COUNT_ERROR = "A1441";

    //////////////////////////////////////登录模块A1100~A1199///////////////////////////////////////////
    /**
     * 登陆失败
     */
    public static final String SEND_MSG_ERROR = "A1101";
    /**
     * 登陆失败
     */
    public static final String LOGIN_ERROR = "A1102";
    /**
     * 验证码验证失败
     */
    public static final String CHECK_MSG_ERROR = "A1103";
    /**
     * 登出失败
     */
    public static final String LOGIN_OUT_ERROR = "A1104";
    /**
     * 登陆失败
     */
    public static final String PHONE_FORMAT_ERROR = "A1105";
    //////////////////////////////////////个人中心A1200~A1299///////////////////////////////////////////
    /*===================================地址列表=====================================*/
    /**
     * 用户地址列表获取失败
     */
    public static final String USER_SELECT_ADDRESS_BOOK_LIST_ERROR = "A1201";
    /**
     * 用户地址添加失败
     */
    public static final String USER_ADD_ADDRESS_BOOK_ERROR = "A1202";
    /**
     * 用户地址添加时收货人为空
     */
    public static final String USER_ADD_ADDRESS_CONSIGNEE_IS_NULL = "A1203";
    /**
     * 用户地址添加时性别为空
     */
    public static final String USER_ADD_ADDRESS_SEX_IS_NULL = "A1204";
    /**
     * 用户地址添加时手机号为空
     */
    public static final String USER_ADD_ADDRESS_PHONE_IS_NULL = "A1205";
    /**
     * 用户地址添加时手机号不正确
     */
    public static final String USER_ADD_ADDRESS_PHONE_IS_FALSE = "A1206";
    /**
     * 用户地址添加时详细地址为空
     */
    public static final String USER_ADD_ADDRESS_DETAIL_IS_NULL = "A1207";
    /**
     * 用户修改默认地址失败
     */
    public static final String USER_UPDATE_DEFAULT_ADDRESS_BOOK_ERROR = "A1208";
    /**
     * 用户查询指定地址失败
     */
    public static final String USER_SELECT_ADDRESS_BOOK_ERROR = "A1209";
    /**
     * 用户删除指定地址失败
     */
    public static final String USER_DELETE_ADDRESS_BOOK_ERROR = "A1210";
    /**
     * 用户修改指定地址信息失败
     */
    public static final String USER_UPDATE_ADDRESS_BOOK_ERROR = "A1211";
    /**
     * 用户地址数量达到最大上限
     */
    public static final String USER_ADDRESS_BOOK_COUNT_MAX = "A1212";
    //////////////////////////////////////登录模块A0700-A0799///////////////////////////////////////////
    /**
     * 订单提交失败
     */
    public static final String ORDER_SUBMIT_ERROR = "A0700";
    /**
     * 无法再来一单
     */
    public static final String ORDER_AGAIN_ERROR = "A0701";
}
