package com.itheima.constant;

/**
 * @Author: Eleike
 * @Desc: 公用静态常量资源
 * @Date: 2022/6/5 21:42
 */
public class CommonConstant {
    /**
     * 分页查询默认当前页数
     */
    public static final Integer PAGE_DEFAULT_CURRENT = 1;
    /**
     * 分页查询默认每页记录数
     */
    public static final Integer PAGE_DEFAULT_PAGESIZE = 10;
    /**
     * 逻辑删除
     */
    public static final int LOGIC_DELETE = 1;
    /**
     * 用户默认地址状态-是
     */
    public static final int USER_DEFAULT_ADDRESS = 1;
    /**
     * 用户默认地址状态-否
     */
    public static final int USER_NOT_DEFAULT_ADDRESS = 0;
    /**
     * 用户最大地址数量
     */
    public static final int USER_MAX_ADDRESS_COUNT = 3;
}
