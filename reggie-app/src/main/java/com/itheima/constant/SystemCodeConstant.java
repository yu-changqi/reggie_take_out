package com.itheima.constant;

/**
 * @Author: Eleike
 * @Desc: 系统错误状态码
 * @Date: 2022/6/5 15:04
 */
public class SystemCodeConstant {
    /**
     * 系统未知错误
     */
    public static final String SYSTEM_UNKNOW_ERROR= "B1000";
}
