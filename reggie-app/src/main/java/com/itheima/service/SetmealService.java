package com.itheima.service;

import com.itheima.common.Result;

/**
 * @Author: jingHaoHao
 * @Desc:套餐管理系统--Service层
 * @Date: 2022/6/6 11:02
 */
public interface SetmealService {

    /**
     * 功能描述  : 菜品和套餐详细信息查询
     * @param id
     * @return : com.itheima.common.Result
     */
    Result findById(Long id);
    /**
     * 功能描述  :
     * @param categoryId
     * @param status
     * @return :
     */
    Result findStatus(Long categoryId, Integer status);
}
