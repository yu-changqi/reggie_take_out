package com.itheima.service;

import com.itheima.vo.OrderVO;
import com.itheima.entity.Order;

import java.util.List;

/**
 * @Author: zhangheng
 * @Desc: 订单模块-业务接口
 * @Date: 2022-06-09 11:21:45
 */
public interface OrderService {
    /*
     * 功能描述:提交订单
     * @return : int
     * @param orders
     */
    int submitOrder(Order orders);

    /**
     * 功能描述: 查询订单列表
     *
     * @param page
     * @param pageSize
     * @return : java.util.List<com.itheima.domain.OrderVO>
     */
    List<OrderVO> page(Integer page, Integer pageSize);
    /*
     * 功能描述:再来一单
     * @return : id
     * @param  Order
     */
    Order againList(Long id);
}
