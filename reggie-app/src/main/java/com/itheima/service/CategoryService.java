package com.itheima.service;


import com.itheima.common.Result;

/**
 * @Author: JingHaoHao
 * @Desc:分类-Service层
 * @Date: 2022/6/8 15:40
 */

public interface CategoryService {
    Result findAll();
}
