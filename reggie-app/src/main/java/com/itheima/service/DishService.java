package com.itheima.service;

import com.itheima.common.Result;

/**
 * @Author: jingHaoHao
 * @Desc: 菜品-Service
 * @Date: 2022/6/8 16:29
 */

public interface DishService {


    /**
     * 功能描述  :通过菜品分类id查询菜品
     * @param
     * @return :
     */
    Result findDish(Long categoryId, Integer status);
}
