package com.itheima.service;

import com.itheima.entity.AddressBook;

import java.util.List;

/**
 * @Author: Eleike
 * @Desc: 用户地址service接口
 * @Date: 2022/6/8 21:01
 */
public interface AddressBookService {
    /**
     * 方法描述 获取用户地址列表
     * @since: 1.0.0
     * @param:
     * @return: List<AddressBook>
     * @author: Eleike
     * @date: 2022/6/9
     */
    List<AddressBook> selectAddressBookListByUserId();
    /**
     * 方法描述 用户地址添加
     * @since: 1.0.0
     * @param: addressBook
     * @return: int
     * @author: Eleike
     * @date: 2022/6/9
     */
    int addAddressBookByUserId(AddressBook addressBook) throws IllegalAccessException;

    /**
     * 方法描述 修改用户默认地址
     * @since: 1.0.0
     * @param: addressBook
     * @return: int
     * @author: Eleike
     * @date: 2022/6/9
     */
    int updateDefaultAddressBookByUserId(AddressBook addressBook);

    /**
     * 方法描述 获取指定地址信息
     * @since: 1.0.0
     * @param: id
     * @return: AddressBook
     * @author: Eleike
     * @date: 2022/6/9
     */
    AddressBook selectUserAddressBookByAddressId(Long id);

    /**
     * 方法描述 删除用户地址
     * @since: 1.0.0
     * @param: ids
     * @return: int
     * @author: Eleike
     * @date: 2022/6/9
     */
    int deleteUserAddressBookByAddressIds(Long[] ids);

    /**
     * 方法描述 修改用户地址信息
     * @since: 1.0.0
     * @param: addressBook
     * @return: int
     * @author: Eleike
     * @date: 2022/6/9
     */
    int updateUserAddressBookByAddress(AddressBook addressBook) throws IllegalAccessException;

    /**
     * 方法描述 或清湖默认地址
     * @since: 1.0.0
     * @param:
     * @return: AddressBook
     * @author: Eleike
     * @date: 2022/6/11
     */
    AddressBook getDefaultAddressBookByUserId();

    /**
     * 方法描述 检测用户地址数量
     * @since: 1.0.0
     * @param:
     * @return: Integer
     * @author: Eleike
     * @date: 2022/6/11
     */
    Integer getUserAddressCount();
}
