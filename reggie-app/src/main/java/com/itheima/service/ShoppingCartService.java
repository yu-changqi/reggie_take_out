package com.itheima.service;

import com.itheima.entity.ShoppingCart;
import java.util.List;

/**
 * @Author: GengZhiFei
 * @Desc: 【瑞吉-app端】-【购物车模块】-【购物车Service接口】
 * @Date: 2022/6/8 14:49
 */
public interface ShoppingCartService {
    /**
     * 功能描述: 将菜品添加到购物车
     * @param shoppingCart
     * @return
     */
    ShoppingCart addShoppingCart(ShoppingCart shoppingCart);
    /**
     * 功能描述: 购物车列表展示
     * @return : java.util.List<com.itheima.entity.ShoppingCart>
     */
    List<ShoppingCart> getShoppingCartListData();
    /**
     * 功能描述: 清空购物车
     * @return
     */
    int emptyTheShoppingCartAllData();
    /**
     * 功能描述: 减少菜品数量, 大于 1份减 1, 等于 1份(物理)删除
     * @param shoppingCart
     * @return
     */
    ShoppingCart reduceDishCount(ShoppingCart shoppingCart);
}
