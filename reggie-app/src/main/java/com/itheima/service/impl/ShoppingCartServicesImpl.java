package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.entity.ShoppingCart;
import com.itheima.mapper.ShoppingCartMapper;
import com.itheima.service.ShoppingCartService;
import com.itheima.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: GengZhiFei
 * @Desc: 【瑞吉-app端】-【购物车模块】-【购物车业务实现类】
 * @Date: 2022/6/8 14:52
 */
@Service
public class ShoppingCartServicesImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    @Override
    public ShoppingCart addShoppingCart(ShoppingCart shoppingCart) {
        //查询数据库购物车数据
        ShoppingCart shoppingCartResult = null;
        //影响行数--可能是修改影响、新增影响
        int row = 0;
        //判断新增数据是否为空
        if (shoppingCart == null) {
            return null;
        }
        //取出菜品ID 套餐ID 并判断是否为空
        Long dishId = shoppingCart.getDishId();
        Long setmealId = shoppingCart.getSetmealId();
        if (dishId == null && setmealId == null) {
            return null;
        }
        //获取当前登录用户ID
        Long threadID = ThreadLocalUtil.getThread();
        //根据userId、dishId、setmealId、dishFlavor查询购物车中菜品是否已经存在
        LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper = new LambdaQueryWrapper<>();
        shoppingCartWrapper.eq(ShoppingCart::getUserId, threadID);
        //拼接套餐ID条件
        if (setmealId != null) {
            shoppingCartWrapper.eq(ShoppingCart::getSetmealId, setmealId);
        }
        //拼接菜品ID条件
        if (dishId != null) {
            shoppingCartWrapper.eq(ShoppingCart::getDishId, dishId);
            String dishFlavor = shoppingCart.getDishFlavor();
            //如果是菜品，还需添加口味条件
            if (dishFlavor == null) {
                return null;
            } else {
                //菜品必须要有口味
                shoppingCartWrapper.eq(ShoppingCart::getDishFlavor, dishFlavor);
            }
        }
        //进行查询
        shoppingCartResult = shoppingCartMapper.selectOne(shoppingCartWrapper);
        //存在则修改数量（number+1）
        if (shoppingCartResult != null) {
            Integer number = shoppingCartResult.getNumber();
            number = number + 1;
            shoppingCartResult.setNumber(number);
            row = shoppingCartMapper.updateById(shoppingCartResult);
        } else {
            //不存在则新增数据
            shoppingCartResult = shoppingCart;
            shoppingCartResult.setCreateTime(LocalDateTime.now());
            shoppingCartResult.setUserId(threadID);

            row = shoppingCartMapper.insert(shoppingCartResult);
        }
        if (row == 0) {
            return null;
        }
        //返回结果
        return shoppingCartResult;
    }

    @Override
    public List<ShoppingCart> getShoppingCartListData() {
        ArrayList<ShoppingCart> list = null;
        //获取用户id
        Long userid = ThreadLocalUtil.getThread();
        if (userid != null) {
            list = new ArrayList<>();
        }
        //封装查询
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userid);
        //根据时间排序
        queryWrapper.orderByDesc(ShoppingCart::getCreateTime);
        //执行查询
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectList(queryWrapper);
        return shoppingCarts;
    }

    @Override
    public int emptyTheShoppingCartAllData() {
        int row = 0;
        //拿当前线程ID
        Long thread = ThreadLocalUtil.getThread();
        if (thread < 1) {
            return row;
        }
        //根据线程ID 清空对应购物车
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, thread);
        row = shoppingCartMapper.delete(queryWrapper);
        return row;
    }


    @Override
    public ShoppingCart reduceDishCount(ShoppingCart shoppingCart) {
        int row=0;
        //拿当前线程ID, 菜品ID, 套餐ID, 菜品口味ID
        Long userID = ThreadLocalUtil.getThread();
        Long dishId = shoppingCart.getDishId();
        Long setmealId = shoppingCart.getSetmealId();
        String dishFlavor = shoppingCart.getDishFlavor();

        if (dishId==null&&setmealId==null){
            return null;
        }
        //封装查询条件
        LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper = new LambdaQueryWrapper<>();
        //根据当前线程ID 查询当前用户购物车
        shoppingCartWrapper.eq(ShoppingCart::getUserId,userID);
        //如果菜品ID不为空
        if (dishId!=null){
            //封装菜品ID, 菜品口味ID 查询条件
            if(dishFlavor!=null){
                shoppingCartWrapper.eq(ShoppingCart::getDishFlavor,dishFlavor);
            }
            shoppingCartWrapper.eq(ShoppingCart::getDishId,dishId);
        }
        //如果套餐ID不为空
        if (setmealId!=null){
            //封装套餐ID 查询条件
            shoppingCartWrapper.eq(ShoppingCart::getSetmealId,setmealId);
        }
        //获得查询结果
        ShoppingCart data = shoppingCartMapper.selectOne(shoppingCartWrapper);
        //获得结果数据的 份数
        Integer number = data.getNumber();
        //如果份数>1, 份数-1
        if (number>1){
            number=number-1;
            data.setNumber(number);
            row = shoppingCartMapper.updateById(data);
        }else{
            //否则, 删除该条数据
            row = shoppingCartMapper.delete(shoppingCartWrapper);
        }
        if (row==0){
            return null;
        }
        return data;

    }
}
