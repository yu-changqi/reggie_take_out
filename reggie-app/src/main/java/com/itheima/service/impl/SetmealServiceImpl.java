package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.entity.Dish;
import com.itheima.entity.Setmeal;
import com.itheima.entity.SetmealDish;
import com.itheima.mapper.DishMapper;
import com.itheima.mapper.SetmealDishMapper;
import com.itheima.mapper.SetmealMapper;
import com.itheima.service.SetmealService;
import com.itheima.vo.SetmealDishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: jingHaoHao
 * @Desc:套餐管理系统--Service层实现
 * @Date: 2022/6/6 11:02
 */
@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    @Autowired
    private DishMapper dishMapper;

    @Override
    public Result findById(Long id) {

        //通过传入的id去查询套餐与菜品的关联表
        LambdaQueryWrapper<SetmealDish> setmealDishWrapper = new LambdaQueryWrapper<>();
        setmealDishWrapper.eq(SetmealDish::getSetmealId, id);
        setmealDishWrapper.orderByDesc(SetmealDish::getUpdateTime);
        List<SetmealDish> setmealDishes = setmealDishMapper.selectList(setmealDishWrapper);
        //初始化试图对象集合
        List<SetmealDishVO> setmealDishVOList = new ArrayList<>();
        //遍历查询到的关联表的集合，并把关联表中的数据拷贝到视图集合中
        for (SetmealDish setmealDish : setmealDishes) {
            SetmealDishVO targetSetmealDishVO = new SetmealDishVO();
            BeanUtils.copyProperties(setmealDish, targetSetmealDishVO);
            setmealDishVOList.add(targetSetmealDishVO);
        }
        //遍历视图集合把缺少的数据补全
        for (SetmealDishVO setmealDishVO : setmealDishVOList) {
            //通过关联表中的菜品id去查询菜品
            Long dishId = setmealDishVO.getDishId();
            LambdaQueryWrapper<Dish> dishWrapper = new LambdaQueryWrapper<>();
            dishWrapper.eq(Dish::getId, dishId);
            //把查询到的菜品中的菜品描述给赋值视图中
            Dish dish = dishMapper.selectOne(dishWrapper);
            setmealDishVO.setDescription(dish.getDescription());
        }
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SETMEAL_DETAILED_GET_SUCCESS, setmealDishVOList);
    }

    @Override
    public Result findStatus(Long categoryId, Integer status) {
        //通过传入的分类id和status，进行查询套餐
        LambdaQueryWrapper<Setmeal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Setmeal::getCategoryId, categoryId);
        wrapper.eq(Setmeal::getStatus, status);
        wrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> setmeals = setmealMapper.selectList(wrapper);
        //返回数据
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.SETMEAL_GET_SUCCESS, setmeals);
    }
}
