package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.constant.CommonConstant;
import com.itheima.entity.AddressBook;
import com.itheima.exception.HeimaExceptionThrow;
import com.itheima.mapper.AddressBookMapper;
import com.itheima.service.AddressBookService;
import com.itheima.utils.InputParameterDetectionUtil;
import com.itheima.utils.StringTrimUtil;
import com.itheima.utils.ThreadLocalUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Eleike
 * @Desc: 用户订单service实现
 * @Date: 2022/6/8 21:02
 */
@Service
public class AddressBookServiceImpl implements AddressBookService {

    @Autowired
    private AddressBookMapper addressBookMapper;

    @Override
    public List<AddressBook> selectAddressBookListByUserId() {
        Long userId = ThreadLocalUtil.getThread();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId,userId);
        queryWrapper.orderByDesc(AddressBook::getIsDefault);
        List<AddressBook> addressBookList = addressBookMapper.selectList(queryWrapper);
        return addressBookList;
    }

    @Override
    public int addAddressBookByUserId(AddressBook addressBook) throws IllegalAccessException {
        int rows = 0;
        if (addressBook == null){
            return rows;
        }
        //去除属性空格
        addressBook = StringTrimUtil.BeanStringAttributeTrimUtil(addressBook);
        //判断收货人是否存在
        if (StringUtils.isBlank(addressBook.getConsignee())){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_CONSIGNEE_IS_NULL, BusinessMessageConstant.USER_ADD_ADDRESS_CONSIGNEE_IS_NULL);
        }
        //判断性别是否选择
        if (addressBook.getSex() == null){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_SEX_IS_NULL, BusinessMessageConstant.USER_ADD_ADDRESS_SEX_IS_NULL);
        }
        //判断手机号是否存在
        if (StringUtils.isBlank(addressBook.getPhone())){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_PHONE_IS_NULL, BusinessMessageConstant.USER_ADD_ADDRESS_PHONE_IS_NULL);
        }
        //检测手机号是否正确
        if (!InputParameterDetectionUtil.phoneNumberDetection(addressBook.getPhone())){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_PHONE_IS_FALSE,BusinessMessageConstant.USER_ADD_ADDRESS_PHONE_IS_FALSE);
        }
        //判断详细地址是否存在
        if (StringUtils.isBlank(addressBook.getDetail())){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_DETAIL_IS_NULL, BusinessMessageConstant.USER_ADD_ADDRESS_DETAIL_IS_NULL);
        }
        Long userId = ThreadLocalUtil.getThread();
        addressBook.setUserId(userId);
        rows = addressBookMapper.insert(addressBook);
        return rows;
    }

    @Override
    public int updateDefaultAddressBookByUserId(AddressBook addressBook) {
        int rows = 0;
        if (addressBook == null || addressBook.getId() == null){
            return rows;
        }
        Long userId = ThreadLocalUtil.getThread();
        LambdaUpdateWrapper<AddressBook> updateWrapper = new LambdaUpdateWrapper<>();
        //先将用户所有地址设为非默认
        updateWrapper.eq(AddressBook::getUserId,userId);
        updateWrapper.set(AddressBook::getIsDefault,CommonConstant.USER_NOT_DEFAULT_ADDRESS);
        addressBookMapper.update(null,updateWrapper);
        //设置指定地址为默认地址
        updateWrapper.clear();
        updateWrapper.eq(AddressBook::getUserId,userId);
        updateWrapper.eq(AddressBook::getId,addressBook.getId());
        addressBook.setIsDefault(CommonConstant.USER_DEFAULT_ADDRESS);
        rows = addressBookMapper.update(addressBook,updateWrapper);
        return rows;
    }

    @Override
    public AddressBook selectUserAddressBookByAddressId(Long id) {
        AddressBook addressBook = null;
        if (id == null){
            return addressBook;
        }
        Long userId = ThreadLocalUtil.getThread();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId,userId);
        queryWrapper.eq(AddressBook::getId,id);
        addressBook = addressBookMapper.selectOne(queryWrapper);
        return addressBook;
    }

    @Override
    public int deleteUserAddressBookByAddressIds(Long[] ids) {
        int rows = 0;
        if (ids == null || ids.length < 1){
            return rows;
        }
        Long userId = ThreadLocalUtil.getThread();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId,userId);
        queryWrapper.in(AddressBook::getId,ids);
        rows = addressBookMapper.delete(queryWrapper);
        return rows;
    }

    @Override
    public int updateUserAddressBookByAddress(AddressBook addressBook) throws IllegalAccessException {
        int rows = 0;
        if (addressBook == null){
            return rows;
        }
        addressBook = StringTrimUtil.BeanStringAttributeTrimUtil(addressBook);
        //判断收货人是否存在
        if (StringUtils.isBlank(addressBook.getConsignee())){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_CONSIGNEE_IS_NULL, BusinessMessageConstant.USER_ADD_ADDRESS_CONSIGNEE_IS_NULL);
        }
        //判断性别是否选择
        if (addressBook.getSex() == null){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_SEX_IS_NULL, BusinessMessageConstant.USER_ADD_ADDRESS_SEX_IS_NULL);
        }
        //判断手机号是否存在
        if (StringUtils.isBlank(addressBook.getPhone())){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_PHONE_IS_NULL, BusinessMessageConstant.USER_ADD_ADDRESS_PHONE_IS_NULL);
        }
        //检测手机号是否正确
        if (!InputParameterDetectionUtil.phoneNumberDetection(addressBook.getPhone())){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_PHONE_IS_FALSE,BusinessMessageConstant.USER_ADD_ADDRESS_PHONE_IS_FALSE);
        }
        //判断详细地址是否存在
        if (StringUtils.isBlank(addressBook.getDetail())){
            HeimaExceptionThrow.throwBusinessException(BusinessCodeConstant.USER_ADD_ADDRESS_DETAIL_IS_NULL, BusinessMessageConstant.USER_ADD_ADDRESS_DETAIL_IS_NULL);
        }
        //判断地址id是否存在
        if (addressBook.getId() == null){
            return rows;
        }
        Long userId = ThreadLocalUtil.getThread();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId,userId);
        queryWrapper.eq(AddressBook::getId,addressBook.getId());
        AddressBook userAddress = addressBookMapper.selectOne(queryWrapper);
        if (userAddress == null){
            return rows;
        }
        rows = addressBookMapper.updateById(addressBook);
        return rows;
    }

    @Override
    public AddressBook getDefaultAddressBookByUserId() {
        Long userId = ThreadLocalUtil.getThread();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId,userId);
        queryWrapper.eq(AddressBook::getIsDefault,CommonConstant.USER_DEFAULT_ADDRESS);
        AddressBook addressBook = addressBookMapper.selectOne(queryWrapper);
        return addressBook;
    }

    @Override
    public Integer getUserAddressCount() {
        Long userId = ThreadLocalUtil.getThread();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId,userId);
        Integer count = addressBookMapper.selectCount(queryWrapper);
        return count;
    }

}
