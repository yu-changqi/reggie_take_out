package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.utils.ThreadLocalUtil;
import com.itheima.entity.*;
import com.itheima.mapper.*;
import com.itheima.service.OrderService;
import com.itheima.vo.OrderVO;
import com.sun.prism.impl.BaseContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zhangHeng
 * @Desc:订单模块-业务实现
 * @Date: 2022/6/8 17:11
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private AddressBookMapper addressMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderDetailMapper orderDetailMapper;


    @Transactional
    @Override
    public int submitOrder(Order orders) {
        int row = 0;
        if(orders==null){
            return row;
        }
        //填充orders数据
        //user中的数据
        Long userId = ThreadLocalUtil.getThread();
        User user = userMapper.selectById(userId);
        String name = user.getName();
        LambdaQueryWrapper<AddressBook> wrapper = new LambdaQueryWrapper<>();
        //地址中的数据
        wrapper.eq(AddressBook::getId, orders.getAddressBookId());
        AddressBook addressBook = addressMapper.selectOne(wrapper);
        String phone = addressBook.getPhone();
        String detail = addressBook.getDetail();
        String consignee = addressBook.getConsignee();

        //购物车中的数据
        LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper = new LambdaQueryWrapper<>();
        shoppingCartWrapper.eq(ShoppingCart::getUserId,userId);
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectList(shoppingCartWrapper);
        int money = 0;
        for (ShoppingCart shoppingCart : shoppingCarts) {
            money = money + shoppingCart.getAmount().intValue()*shoppingCart.getNumber();
        }

        orders.setUserId(userId);
        orders.setOrderTime(LocalDateTime.now());
        orders.setCheckoutTime(LocalDateTime.now());
        orders.setAmount(new BigDecimal(money));
        orders.setPhone(phone);
        orders.setAddress(detail);
        orders.setUserName(name);
        orders.setConsignee(consignee);
        row = orderMapper.insert(orders);
        if (row<1){
            return row;
        }
        //查出购物车中的数据 ,并插入到订单明细中
        for (ShoppingCart shoppingCart : shoppingCarts) {
            OrderDetail orderDetail = new OrderDetail();
            BeanUtils.copyProperties(shoppingCart,orderDetail);
            orderDetail.setId(null);
            orderDetail.setOrderId(orders.getId());
            orderDetailMapper.insert(orderDetail);
        }
        //清空购物车
        LambdaQueryWrapper<ShoppingCart> shoppingWrapper = new LambdaQueryWrapper<>();
        shoppingWrapper.eq(ShoppingCart::getUserId,userId);
        shoppingCartMapper.delete(shoppingWrapper);

        return row;
    }
    @Override
    public List<OrderVO> page(Integer page, Integer pageSize) {
        List<OrderVO> orderVOList = null;
        //1.校验参数
        if (page < 1) {
            page = 1;
        }
        if (pageSize < 1) {
            pageSize = 10;
        }
        //2.查询订单列表---根据userID
        Page<Order> ordersPage = new Page<>(page, pageSize);
        Long userId = ThreadLocalUtil.getThread();
        LambdaQueryWrapper<Order> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Order::getUserId, userId);
        wrapper.orderByDesc(Order::getCheckoutTime, Order::getOrderTime);
        Page<Order> pageResult = orderMapper.selectPage(ordersPage, wrapper);
        List<Order> records = pageResult.getRecords();
        if (records == null || records.size() == 0) {
            return orderVOList;
        }
        orderVOList = new ArrayList<>();
        for (Order record : records) {
            OrderVO orderVO = new OrderVO();
            BeanUtils.copyProperties(record, orderVO);
            //根据订单ID查询订单明细
            LambdaQueryWrapper<OrderDetail> orderDetailWrapper = new LambdaQueryWrapper<>();
            orderDetailWrapper.eq(OrderDetail::getOrderId, orderVO.getId());
            List<OrderDetail> orderDetails = orderDetailMapper.selectList(orderDetailWrapper);
            orderVO.setOrderDetails(orderDetails);
            //封装对象
            orderVOList.add(orderVO);
        }
        return orderVOList;
    }

    @Override
    public Order againList(Long id) {
        if(id==null){
            return null;
        }
        LambdaQueryWrapper<Order> orderLambdaQueryWrapper = new LambdaQueryWrapper<>();
        orderLambdaQueryWrapper.eq(Order::getId,id);
        Order order = orderMapper.selectOne(orderLambdaQueryWrapper);
        return order;
    }
}
