package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.common.Result;
import com.itheima.constant.BusinessCodeConstant;
import com.itheima.constant.BusinessMessageConstant;
import com.itheima.entity.Dish;
import com.itheima.entity.DishFlavor;
import com.itheima.mapper.DishFlavorsMapper;
import com.itheima.mapper.DishMapper;
import com.itheima.service.DishService;
import com.itheima.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {

    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorsMapper dishFlavorsMapper;
    @Override
    public Result findDish(Long categoryId, Integer status) {
        //通过传入的分类id和status，进行查询菜品
        LambdaQueryWrapper<Dish> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Dish::getCategoryId,categoryId);
        wrapper.eq(Dish::getStatus,status);
        wrapper.orderByDesc(Dish::getUpdateTime);
        List<Dish> dishes = dishMapper.selectList(wrapper);
        //初始化菜品视图集合
        List<DishVO> dishVOS =new ArrayList<>();
        //遍历查询到的菜品集合
        for (Dish dish : dishes) {
            //初始化一个视图对象，并把查询到的菜品数据拷贝到视图中
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(dish,dishVO);
            //查询菜品口味表
            LambdaQueryWrapper<DishFlavor> dishFlavorWrapper = new LambdaQueryWrapper<>();
            dishFlavorWrapper.eq(DishFlavor::getDishId,dish.getId());
            List<DishFlavor> dishFlavors = dishFlavorsMapper.selectList(dishFlavorWrapper);
            //判断口味是否为空如果为空的话就不需要赋值，
            // 直接把视图添加到视图集合中
            if (dishFlavors!=null){
                //把口味集合赋值给视图，在把视图添加到视图集合中
                dishVO.setFlavors(dishFlavors);
            }
             dishVOS.add(dishVO);
        }
        return new Result(BusinessCodeConstant.OK, BusinessMessageConstant.DISH_GET_SUCCESS,dishVOS);
    }
}
