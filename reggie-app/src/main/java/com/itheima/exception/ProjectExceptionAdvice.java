package com.itheima.exception;

import com.itheima.common.Result;
import com.itheima.constant.SystemCodeConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * ProjectExceptionAdvice
 *  增强Controller层的通知
 * @author lishuo
 * @date 2022/6/8
 */
@RestControllerAdvice
@Slf4j
public class ProjectExceptionAdvice {
    /**
     * 功能描述: 异常处理器
     * @param exception
     * @return : com.itheima.common.Result
     */
    public Result exception(Exception exception){
        if(exception instanceof BusinessException){
            return new Result(((BusinessException)exception).getCode(),exception.getMessage());
        }
        return  new Result(SystemCodeConstant.SYSTEM_UNKNOW_ERROR,exception.getMessage());
    }
}
