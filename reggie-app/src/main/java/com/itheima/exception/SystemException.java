package com.itheima.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * SystemException
 *  自定义系统业务异常
 * @author lishuo
 * @date 2022/6/8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemException extends RuntimeException {
    /**
     * 状态码
     */
    private String code;

    public SystemException(String code, String errMsg) {
    super(errMsg);
    this.code=code;
    }
    public SystemException(String code,String errMsg,Throwable causer){
    super(errMsg,causer);
    this.code=code;
    }
}
