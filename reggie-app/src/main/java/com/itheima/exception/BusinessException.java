package com.itheima.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * BusinessException
 *  自定义业务异常
 * @author lishuo
 * @date 2022/6/8
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusinessException extends  RuntimeException{
    /**
     * 状态码
     */
    private String code;

    public BusinessException(String code,String message){
        super(message);
        this.code = code;
    }
    public BusinessException(String code,String message,Throwable cause){
        super(message,cause);
        this.code=code;
    }
}
